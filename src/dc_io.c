//
// Created by Alberto Scionti on 01/08/2018.
//
#include "dc_io.h"

// functions implementations for input-output
int fitness_plot(char* export, int x_min, int x_max, int y_min, int y_max)
{
    // data
    FILE* export_f;

    // code
    export_f = fopen(export, "w");
    if (export_f == NULL)
    {
        fprintf(stderr, "[ERR] unable to open output file for plotting script\n");
        return FAILURE;
    }
    fprintf(export_f, "#! /usr/bin/gnuplot\n"
                      "set term png size 1024,720 font \"/usr/share/fonts/liberation/LiberationSans-Regular.ttf\" 12\n"
                      "set output '../plots/fitness_track.png'\n\n"
                      "set xlabel 'Generations'\n"
                      "set ylabel 'Schedule efficiency [W]'\n\n"
                      "set grid\n"
                      "set key below center horizontal noreverse enhanced autotitle\n"
                      "set tics out nomirror\n\n"
                      "set xrange [%d:%d]\n"
                      "set xtics rotate\n"
                      "set yrange [%d:%d]\n\n"
                      "set style line 1 linecolor rgb '#99004c' linewidth 1 linetype 3 pointtype 2\n"
                      "set style data linespoints\n\n"
                      "plot '../data/es_dump.txt' using 1:2 linestyle 1 title 'Schedule efficiency'\n",
                      x_min,
                      x_max,
                      y_min,
                      y_max);
    fclose(export_f);
    return SUCCESS;
}
