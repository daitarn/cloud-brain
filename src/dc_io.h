//
// Created by Alberto Scionti on 01/08/2018.
//

#ifndef DC_SOLVER_DC_IO_H
#define DC_SOLVER_DC_IO_H

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>


#define SUCCESS     0
#define FAILURE     1

// function prototypes for input-output
int fitness_plot(char* export, int x_min, int x_max, int y_min, int y_max);


#endif //DC_SOLVER_DC_IO_H
