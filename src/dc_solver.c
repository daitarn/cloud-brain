/* =================================================================
 * WORKLOAD  => vettore con lunghezza pari al numero di task, contiene per ogni entry il
 *              tipo del task
 * TASK_LIST => vettore con lunghezza pari al numero di task, contiene per ogni entry
 *              il nodo su cui e' stato allocato il task
 * DELTA     => vettore di lunghezza pari al numero di server, contiene per ogni entry
 *              il numero di core ancora disponibili
 * SERVER_T  => vettore contenente i tipi di server (#core disponibili per ciascuno)
 * TASK_T    => vettore contenente i tipi di task (#core usati da ciascun tipo di task)
 *
 * nodes.dat => elenco dei server e per ognuno il tipo associato
 * workload.dat => contiene la lista di task (per ogni task contiene il tipo del task)
 * task_list.dat => contiene per ogni task il server su cui e' stato allocato
 *
 *
 * ---------------------------------------------------------------
 * Author: A. Scionti -- Ph.D (Senior Researcher)
 * Institution: ISMB-Links
 * Copyright (C): Ismb-Links - 2018
 * Date: 2018
 * Last modification: 01 August 2018
 * Version 1.03
 * ---------------------------------------------------------------
 *
 *
   ================================================================= */


// Libraries
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <sys/time.h>
//#include <omp.h>


// Local libraries
#include "thpool.h"
#include "dc_io.h"



// Symbolic constants
#define UNIFORM         0
#define NORMAL          1
#define MIXED           2
#define INF             1.0e6
#define SUCCESS         0
#define FAILURE         1
#define FRAC_IDLE       0.65
#define LAMBDA          0.10
#define MUTATION_1      0.1
#define MUTATION_2      0.15
#define MUTATION_3      0.15
#define MUTATION_4      0.25
#define MUTATION_5      0.05
#define false           0
#define true            1


// Function prototypes
int* get_server_types(int no_server_t);
int* get_task_types(int no_task_t);
int* get_server_Mtypes(int no_server_t);
int* get_task_Mtypes(int no_task_t);
double* get_max_power_dc(int no_servers, int no_server_t, char* filename, char* filename2);
//int** get_task_list (int population_size, int no_tasks, char* filename);
int* get_task_list (int no_tasks, char* filename);
int* get_workload (int no_tasks, int* task_t, char* filename);
int** get_delta_server (int no_servers, int no_tasks, int population_size, int** task_list, int* workload, int* max_load, int* task_t);
int* get_delta_load(int no_servers, int* max_load, int* cap);
int* get_max_server(int no_servers, int* server_t, char* filename);
int* get_mem_max_server(int no_servers, int* server_t, char* filename);
int get_config (int* population_size, int* no_tasks, int* no_servers, int* no_task_t, int* no_server_t, char* filename);
int** release_task_list (int population_size, int** task_list);
int* release_workload (int* workload);
int** release_delta_server (int population_size, int** delta_server);
int* release_max_server (int* max_load);
double* release_power_dc(double* power_dc);
int* fresh_node_list(int no_servers, int no_server_t,  int* server_t);
int* get_cap_load(int no_servers, int* max_load);
int* get_cap_memory(int no_servers, int* max_mem);
int* get_delta_memory(int no_servers, int* max_mem, int* cap_mem);
int schedule_BF(int no_servers,
                int no_tasks,
                int no_task_t,
                int no_server_t,
                int* cap_load,
                int* cap_mem,
                int* task_t,
                int* task_Mt,
                int* task_list,
                int* workload);
double summary_power_dc(double* power_vector, int no_servers);
double* get_power_dc(int no_servers, int* max_load, int* cap_load, double* max_power);
struct fresh_load_s get_fresh_load(int* max_load,
                                   int* cap_load,
                                   int no_servers,
                                   int* task_t,
                                   int no_task_t,
                                   int no_tasks,
                                   int* max_mem,
                                   int* cap_mem,
                                   int* task_Mt);
struct fresh_load_s* clone_agents(int population_size, struct fresh_load_s fresh_load);
int dump_agents(struct fresh_load_s* agent, int population_size);
int get_new_parent(int population_size, struct fresh_load_s* agent);
int fast_clone_agents(int population_size, struct fresh_load_s* agent, struct fresh_load_s parent, int skip_agent);
int agent_mutation_1(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt);
int agent_mutation_2(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt);
int agent_mutation_3(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt);
int agent_mutation_4(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt);
int agent_mutation_5(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt);
struct time_diff_s* exec_time(struct timeval* t1, struct timeval* t2);
int consolidate_agent(struct fresh_load_s* agent, int* task_t, int agent_id, int* max_load);
double power_off(struct fresh_load_s schedule, int* max_load, double* max_power_dc, int* max_mem);
//
double agent_efficiency(int agent_id, struct fresh_load_s* agent, int* max_load, double* max_power_dc);
void header_msg(void);
double accounting_migration(struct fresh_load_s src, struct fresh_load_s* dst, int id, double* max_power, int* max_load, int* task_t);
//
int check_schedule(struct fresh_load_s* agent);
int poisson(double mean);
double expon(double x);
double randn(double mu, double sigma);
int pick_task(int task_no, double mu, double sigma);
//int cpy_agent(struct fresh_load_s* src, struct fresh_load_s* dst);


// Global structure definition
struct fresh_load_s
{
    int no_servers;
    int no_tasks;
    int* task_list;
    int* workload;
    int* delta;
    int* deltaM;
    double efficiency;
};


struct work_s
{
    int agent_id;
    int* task_t;
    struct fresh_load_s* agent;
};


struct time_diff_s
{
    int secs;
    int usecs;
};


// Global variables
FILE* es;
//int rand_type = MIXED;
int rand_type = UNIFORM;
int dbg = 1;
int gen_workload = 0;
int generations = 100;
double max_time = 5.0;
double cap = 0.625;
int go_plot = true;
char* server_t_file = "../data/server_t.dat";
char* task_t_file = "../data/task_t.dat";
char* power_dc_file = "../data/server_power.dat";
char* config_file = "../data/configuration.dat";
double mu_1 = MUTATION_1;
double mu_2 = MUTATION_2;
double mu_3 = MUTATION_3;
double mu_4 = MUTATION_4;
double mu_5 = MUTATION_5;
int escape = false;

// Main function
int main(int argc, char** argv)
{
    // Data
    struct time_diff_s* elapsed_time;
    struct timeval myTVstart, myTVend;
    threadpool* workers;
    struct work_s work;
    double *max_power_dc;
    double *power_dc;
    double power_tot;
    double init_eff;
    double final_eff;
    double eff_tmp;
    //double real_time;
    clock_t ini_time;
    clock_t end_time;
    int steady_state;
    double elapsed_t;
    int op;
    int g;
    int test_rnd = false;
    int n;
    int t;
    int p;
    int a;
    int population_size;
    int no_task_t;
    int no_tasks;
    int no_servers;
    int no_server_t;
    //int **task_list;
    int* task_list;
    int* workload;
    int* max_load;
    int* max_mem;
    int* cap_load;
    int* cap_mem;
    int* server_t;
    int* server_Mt;
    int* task_t;
    int* task_Mt;
    //int **delta_server;
    int* delta;
    int* deltaM;
    struct fresh_load_s fresh_load;
    struct fresh_load_s *parent;
    struct fresh_load_s *best_agent;
    struct fresh_load_s *agent;
    struct fresh_load_s *dummy;
    // output
    FILE* summary;




    // Code
    header_msg();
    //
    srand((unsigned int) time(0));
    // read global configuration and parameters
    no_server_t = 0;
    no_task_t = 0;
    population_size = 0;
    no_tasks = 0;
    no_servers = 0;
    gen_workload = false;
    cap_load = NULL;
    task_list = NULL;
    workload = NULL;
    max_mem = NULL;
    cap_mem = NULL;
    deltaM = NULL;
    server_Mt = NULL;
    task_Mt = NULL;
    //delta_server = NULL;
    delta = NULL;
    get_config(&population_size, &no_tasks, &no_servers, &no_task_t, &no_server_t, config_file);
    dbg = 0;
    int dummy_dbg = dbg;
    dbg = true;
    if (dbg)
    {
        fprintf(stdout, "[DBG] debugging mode = enabled\n");
        fprintf(stdout, "[DBG] population_size = %d\n", population_size);
        fprintf(stdout, "[DBG] no_tasks (to read/generate)= %d\n", no_tasks);
        fprintf(stdout, "[DBG] no_servers = %d\n", no_servers);
        fprintf(stdout, "[DBG] no_task_t = %d\n", no_task_t);
        fprintf(stdout, "[DBG] no_server_t = %d\n", no_server_t);
        fprintf(stdout, "[DBG] cap_value = %.3lf\n", cap);
        fprintf(stdout, "[DBG] max generations = %d\n", generations);
        fprintf(stdout, "[DBG] max elapsed time = %.1lf\n", max_time);
        fprintf(stdout, "[DBG] mutation rate (operator 1) = %.3lf\n", mu_1);
        fprintf(stdout, "[DBG] mutation rate (operator 2) = %.3lf\n", mu_2);
        fprintf(stdout, "[DBG] mutation rate (operator 3) = %.3lf\n", mu_3);
        fprintf(stdout, "[DBG] mutation rate (operator 4) = %.3lf\n", mu_4);
        fprintf(stdout, "[DBG] mutation rate (operator 5) = %.3lf\n", mu_5);
        fprintf(stdout, "[DBG] gen_workload = ");
        if (gen_workload == true)
        {
            fprintf(stdout, "True\n");
        }
        else
        {
            fprintf(stdout, "False\n");
        }
    }
    dbg = dummy_dbg;
    // get the types of server_nodes and tasks
    server_t = get_server_types(no_server_t);
    task_t = get_task_types(no_task_t);
    server_Mt = get_server_Mtypes(no_server_t);
    task_Mt = get_task_Mtypes(no_task_t);
    work.task_t = task_t;
    dummy_dbg = dbg;
    dbg = false;
    if (dbg)
    {
        fprintf(stdout, "[Server types] %d\n", no_server_t);
        for (t = 0; t < no_server_t; t++)
            fprintf(stdout, "[Server_type %d] = %d(vcpu) : %d(GiB)\n", t, server_t[t], server_Mt[t]);
        fprintf(stdout, "[Task types] %d\n", no_task_t);
        for (t = 0; t < no_task_t; t++)
            fprintf(stdout, "[Task_type %d] = %d(vcpu) : %d(GiB)\n", t, task_t[t], task_Mt[t]);
    }
    if (test_rnd == true)
    {
        fprintf(stdout, "\n\n*******************\n");
        int pd;
        for (n = 0; n < 35; n++) {
            pd = pick_task(14, 0.0, 0.70);
            fprintf(stdout, "%d ", pd - 1);
        }
        fprintf(stdout, "\n\n*******************\n");
    }
    if (dbg == 2)
    {
        for (t = 0; t < no_server_t; t++)
        {
            fprintf(stdout, "[DBG] server type (%d): %d GiB\n", t, server_Mt[t]);
        }
        for (t = 0; t < no_task_t; t++)
        {
            fprintf(stdout, "[DBG] task type (%d): %d GiB\n", t, task_Mt[t]);
        }
    }
    dbg = dummy_dbg;
    // ***************************************************
    // Read workload from file
    // ***************************************************
    if (gen_workload == false)
    {
        // get the workload: this is an array with length equals to the number of task to allocate
        // the index of the array is the task_id, while the content is the number of resources required (cpu cores)
        fprintf(stdout, "[INF] reading workload list\n");
        workload = get_workload(no_tasks, task_t, "../data/workload.dat");
        if (workload == NULL) {
            fprintf(stderr, "[ERR] memory allocator (workload)\n");
            return FAILURE;
        }
        if (dbg)
        {
            fprintf(stdout, "[DBG] WORKLOAD ( task_id,  cpu_core_required )\n");
            for (t = 0; t < no_tasks; t++)
                fprintf(stdout, "task(%d): %d\n", t, workload[t]);
        }
        // get the maximum capacity of the server
        fprintf(stdout, "[INF] reading server max load\n");
        max_load = get_max_server(no_servers, server_t, "../data/nodes.dat");
        max_mem = get_mem_max_server(no_servers, server_Mt, "../data/nodes.dat");
        dummy_dbg = dbg;
        dbg = false;
        if (dbg)
        {
            fprintf(stdout, "[DBG] MAX_LOAD/MEM ( server_id,  VCPUs,  Memory )\n");
            for (n = 0; n < no_servers; n++)
            {
                fprintf(stdout, "      node(%d) = %d(vcpu) : %d(GiB)\n", n, max_load[n], max_mem[n]);
            }
            //fprintf(stdout, "[DBG] MAX_MEM ( server_id,  memory_available )\n");
            //for (n = 0; n < no_servers; n++)
            //{
            //    fprintf(stdout, "      node(%d): %d\n", n, max_mem[n]);
            //}
        }
        dbg = dummy_dbg;
        //
        //
        // get the task list - population
        // task_list = get_task_list(population_size, no_tasks, "../data/task_list.dat");
        fprintf(stdout, "[INF] reading task list\n");
        task_list = get_task_list(no_tasks, "../data/task_list.dat");
        if (task_list == NULL)
        {
            fprintf(stderr, "[ERR] memory allocator (task_list)\n");
            return FAILURE;
        }
        if (dbg)
        {
            int dummy_p_size = 1;
            for (p = 0; p < dummy_p_size ; p++) {
                fprintf(stdout, "agent %d =>\n", p);
                for (t = 0; t < no_tasks; t++) {
                    if ((t % 20 == 0) && (t != 0))
                        fprintf(stdout, "\n");
                    // fprintf(stdout, "%3d ", task_list[p][t]);
                    fprintf(stdout, "%3d ", task_list[t]);
                }
                fprintf(stdout, "\n");
            }
        }
        // generate cap load vector
        fprintf(stdout, "[INF] compute the cap load vector\n");
        cap_load = get_cap_load(no_servers, max_load);
        cap_mem = get_cap_memory(no_servers, max_mem);
        dummy_dbg = dbg;
        dbg = false;
        if (dbg)
        {
            //int dummy_p_size = 1;
            //for (p = 0; p < dummy_p_size; p++) {
            //fprintf(stdout, "delta_server (agent %d) =>\n", p);
            fprintf(stdout, "cap resources server =>\n");
            for (n = 0; n < no_servers; n++)
            {
                if ((n % 20 == 0) && (n != 0))
                    fprintf(stdout, "\n");
                //fprintf(stdout, "%3d ", delta_server[p][n]);
                fprintf(stdout, "%3d(%3d) ", cap_load[n], cap_mem[n]);
            }
            fprintf(stdout, "\n");
            //}
        }
        dbg = dummy_dbg;
        //
        //
        // get the actual server capacity
        fprintf(stdout, "[INF] getting current delta vector\n");
        //delta_server = get_delta_server(no_servers, no_tasks, population_size, task_list, workload, max_load);
        //delta_server = get_delta_server(no_servers, no_tasks, 1, task_list, workload, max_load, task_t);
        delta = get_delta_load(no_servers, max_load, cap_load);
        deltaM = get_delta_memory(no_servers, max_mem, cap_mem);
        if (delta == NULL) {
            fprintf(stderr, "[ERR] memory allocator (delta_server\n)");
            return FAILURE;
        }
        dummy_dbg = dbg;
        dbg = false;
        if (dbg)
        {
            //int dummy_p_size = 1;
            //for (p = 0; p < dummy_p_size; p++) {
                //fprintf(stdout, "delta_server (agent %d) =>\n", p);
                fprintf(stdout, "delta_server =>\n");
                for (n = 0; n < no_servers; n++)
                {
                    if ((n % 20 == 0) && (n != 0))
                        fprintf(stdout, "\n");
                    //fprintf(stdout, "%3d ", delta_server[p][n]);
                    fprintf(stdout, "%3d(%3d) ", delta[n], deltaM[n]);
                }
                fprintf(stdout, "\n");
            //}
        }
        dbg = dummy_dbg;
        //
        // transfer on fresh_load structure
        fprintf(stdout, "[INF] transferring data on fresh_load data structure\n");
        fresh_load.workload = (int*) malloc(sizeof(int) * no_tasks);
        fresh_load.task_list = (int*) malloc(sizeof(int) * no_tasks);
        fresh_load.delta = (int*) malloc(sizeof(int) * no_servers);
        fresh_load.deltaM = (int*) malloc(sizeof(int) * no_servers);
        fresh_load.no_tasks = no_tasks;
        fresh_load.no_servers = no_servers;
        if (fresh_load.workload == NULL || fresh_load.task_list == NULL ||
            fresh_load.delta == NULL || fresh_load.deltaM == NULL)
        {
            fprintf(stderr, "[ERR] memory allocation error (transferring data structure)\n");
            return FAILURE;
        }
        for (t = 0; t < no_tasks; t++)
        {
            fresh_load.workload[t] = workload[t];
            fresh_load.task_list[t] = task_list[t];
        }
        for (n = 0; n < no_servers; n++)
        {
            fresh_load.delta[n] = delta[n];
            fresh_load.deltaM[n] = deltaM[n];
        }
        // print statistics:
        fprintf(stdout, "[ESS] statistics (task types distribution):\n");
        fprintf(stdout, "      --------------------------------\n");
        fprintf(stdout, "      reading tasks\n");
        fprintf(stdout, "      --------------------------------\n");
        int task_counter;
        int total_tasks;
        int k;
        total_tasks = 0;
        for (k = 0; k < no_task_t; k++)
        {
            task_counter = 0;
            for (t = 0; t < no_tasks; t++)
            {
                if (workload[t] == k)
                {
                    task_counter++;
                }
            }
            fprintf(stdout, "      task_class [ %2d ] = %d\n", k, task_counter);
            total_tasks += task_counter;
        }
        fprintf(stdout, "      --------------------------------\n");
        fprintf(stdout, "      total task found = %3d\n", total_tasks);
        fprintf(stdout, "      --------------------------------\n");
        // ======================/ CHECK / ========================
        for (t = 0; t < fresh_load.no_tasks; t++)
        {
            if (t % 25 == 0 && t > 0)
                fprintf(stdout, "\n");
            fprintf(stdout, "%2d ", fresh_load.workload[t]);
        }
        fprintf(stdout, "\n");
        // ======================    //    ========================
    }
    //
    //
    // ***************************************************
    // Generate the workload at fresh
    // ***************************************************
    else
    {
        // get a fresh list of nodes in the data center
        max_load = fresh_node_list(no_servers, no_server_t, server_t);
        max_mem = get_mem_max_server(no_servers, server_Mt, "../data/nodes.dat");
        if (dbg)
        {
            fprintf(stdout, "[DBG] MAX_LOAD ( server_id,  cpu_core_available )\n");
            for (n = 0; n < no_servers; n++)
                fprintf(stdout, "node(%d): %d\n", n, max_load[n]);
        }
        // get capped load on data center
        cap_load = get_cap_load(no_servers, max_load);
        cap_mem = get_cap_memory(no_servers, max_mem);
        if (dbg)
        {
            fprintf(stdout, "[DBG] CAP_LOAD ( server_id,  cpu_core_available )\n");
            for (n = 0; n < no_servers; n++)
                fprintf(stdout, "node(%d): %d\n", n, cap_load[n]);
        }
        // get a fresh list of task to be allocated
        fresh_load = get_fresh_load(max_load,
                                    cap_load,
                                    no_servers,
                                    task_t,
                                    no_task_t,
                                    no_tasks,
                                    max_mem,
                                    cap_mem,
                                    task_Mt);
        if (dbg)
        {
            fprintf(stdout, "[INF] delta resource vector:\n");
            for (n = 0; n < no_servers; n++) {
                if ((n != 0) && (n % 50 == 0)) {
                    fprintf(stdout, "\n");
                }
                fprintf(stdout, "%2d ", fresh_load.delta[n]);
            }
            fprintf(stdout, "\n");
        }
        fresh_load.efficiency = INF;
        //
        // save on file data structure that will be read next time
        summary = fopen("../data/workload.dat", "w");
        if (summary == NULL)
        {
            fprintf(stderr, "[ERR] error opening output summary file (workload.dat)\n");
        }
        else
        {
            for (t = 0; t < fresh_load.no_tasks; t++)
            {
                fprintf(summary, "%d\n", fresh_load.workload[t]);
            }
            fclose(summary);
        }
        summary = fopen("../data/task_list.dat", "w");
        if (summary == NULL)
        {
            fprintf(stderr, "[ERR] error opening output summary file (task_list.dat)\n");
        }
        else
        {
            for (t = 0; t < fresh_load.no_tasks; t++)
            {
                fprintf(summary, "%d\n", fresh_load.task_list[t]);
            }
            fclose(summary);
        }
        fresh_load.no_servers = no_servers;
        summary = fopen("../data/delta.dat", "w");
        if (summary == NULL)
        {
            fprintf(stderr, "[ERR] error opening output summary file (task_list.dat)\n");
        }
        else
        {
            for (n = 0; n < fresh_load.no_servers; n++)
            {
                fprintf(summary, "%d : %d\n", fresh_load.delta[n], fresh_load.deltaM[n]);
            }
            fclose(summary);
        }
        fprintf(stdout, "[ESS] statistics (task types distribution):\n");
        fprintf(stdout, "      --------------------------------\n");
        fprintf(stdout, "      generated tasks\n");
        fprintf(stdout, "      --------------------------------\n");
        int task_counter;
        int total_tasks;
        int k;
        total_tasks = 0;
        for (k = 0; k < no_task_t; k++)
        {
            task_counter = 0;
            for (t = 0; t < no_tasks; t++)
            {
                if (fresh_load.workload[t] == k)
                {
                    task_counter++;
                }
            }
            fprintf(stdout, "      task_class [ %2d ] = %d\n", k, task_counter);
            total_tasks += task_counter;
        }
        fprintf(stdout, "      --------------------------------\n");
        fprintf(stdout, "      total task found = %3d\n", total_tasks);
        fprintf(stdout, "      --------------------------------\n");
        // ======================/ CHECK / ========================
        for (t = 0; t < fresh_load.no_tasks; t++)
        {
            if (t % 25 == 0 && t > 0)
                fprintf(stdout, "\n");
            fprintf(stdout, "%2d ", fresh_load.workload[t]);
        }
        fprintf(stdout, "\n");
        // ======================    //    ========================
    }
    // ***************************************************
    // Get power of data center
    // ***************************************************
    fprintf(stdout, "[INF] collecting initial efficiency\n");
    max_power_dc = get_max_power_dc(no_servers, no_server_t, power_dc_file, "../data/nodes.dat");
    power_tot = summary_power_dc(max_power_dc, no_servers);
    if (dbg) {
        fprintf(stdout, "**********\n[DBG] MAX power consumption of servers:\n");
        fprintf(stdout, "[DBG] Total power consumption: %.3lf W\n", power_tot);
    }
    power_dc = get_power_dc(no_servers, max_load, cap_load, max_power_dc);
    power_tot = summary_power_dc(power_dc, no_servers);
    if (dbg) {
        fprintf(stdout, "**********\n[DBG] power consumption of servers:\n");
        fprintf(stdout, "[DBG] Total power consumption: %.3lf W\n", power_tot);
    }
    fresh_load.efficiency = power_tot;
    //
    //
    //
    // ***************************************************
    // Run the optimization process
    // ***************************************************
    fprintf(stdout, "[INF] RUNNING OPTIMIZATION PROCESS\n");
    es = fopen("../data/es_dump.txt", "w");
    if (go_plot == true)
    {
        fprintf(es, "#generation  fitness\n");
    }
    // Start getting initial population (clone all the agents)
    fresh_load.no_servers = no_servers;
    // getting parent
    parent = clone_agents(1, fresh_load);
    if (dbg == 2) {
        fprintf(es, "[parent]\n");
        fprintf(es, "  no. servers: %d\n", parent->no_servers);
        fprintf(es, "  no. tasks: %d\n", parent->no_tasks);
        fprintf(es, "  efficiency: %.3lf\n", parent->efficiency);
        fprintf(es, "  task-list: ");
        for (int t = 0; t < parent->no_tasks; t++)
        {
            if (t % 50 == 0)
                fprintf(es, "\n");
            fprintf(es, "%2d ", parent->task_list[t]);
        }
        fprintf(es, "\n");
        fprintf(es, "  delta: ");
        for (int n = 0; n < parent->no_servers; n++)
        {
            if (n % 25 == 0)
                fprintf(es, "\n");
            fprintf(es, "VCPU=%2d : MEM=%2d ", parent->delta[n], parent->deltaM[n]);
        }
        fprintf(es, "\n\n");
    }
    //
    //
    power_off(*parent, max_load, max_power_dc, max_mem);
    //parent->efficiency -= eff_tmp;
    //
    // getting best_agent
    best_agent = clone_agents(1, *parent);
    dummy = clone_agents(1, *parent);
    steady_state = 0;
    if (dbg == 2)
    {
        fprintf(es, "[best agent]\n");
        fprintf(es, "  no. servers: %d\n", best_agent->no_servers);
        fprintf(es, "  no. tasks: %d\n", best_agent->no_tasks);
        fprintf(es, "  efficiency: %.3lf\n", best_agent->efficiency);
        fprintf(es, "  task-list: ");
        for (int t = 0; t < best_agent->no_tasks; t++)
            fprintf(es, "%2d ", best_agent->task_list[t]);
        fprintf(es, "\n");
        fprintf(es, "  delta: ");
        for (int n = 0; n < best_agent->no_servers; n++)
            fprintf(es, "%2d ", best_agent->delta[n]);
        fprintf(es, "\n\n");
    }
    // cloning agents
    a = 0;
    agent = clone_agents(population_size, *parent);
    init_eff = agent_efficiency(0, agent, max_load, max_power_dc);
    // ********************
    eff_tmp = accounting_migration(fresh_load, agent, 0, max_power_dc, max_load, task_t);
    init_eff += eff_tmp;
    // ********************
    if (dbg == true)
    {
        fprintf(stderr, "[DBG] migration power %3.lf\n", eff_tmp);
    }
    best_agent->efficiency = init_eff;
    if (dbg) {
        dump_agents(agent, population_size);
    }
    if (dbg == 2) {
        fprintf(es, "[cloning agents]:\n");
        for (int i = 0; i < population_size; i++) {
            fprintf(es, "[agent %d]\n", i);
            fprintf(es, "  no. servers: %d\n", agent[i].no_servers);
            fprintf(es, "  no. tasks: %d\n", agent[i].no_tasks);
            fprintf(es, "  efficiency: %.3lf\n", agent[i].efficiency);
            fprintf(es, "  task-list: ");
            for (int t = 0; t < agent[i].no_tasks; t++)
                fprintf(es, "%2d ", agent[i].task_list[t]);
            fprintf(es, "\n");
            fprintf(es, "  delta (vcpus): ");
            for (int n = 0; n < agent[i].no_servers; n++)
                fprintf(es, "%2d ", agent[i].delta[n]);
            fprintf(es, "\n");
            fprintf(es, "  delta (mem): ");
            for (int n = 0; n < agent[i].no_servers; n++)
                fprintf(es, "%2d ", agent[i].deltaM[n]);
            fprintf(es, "\n");
        }
        fprintf(es, "\n\n========= Start =========\n\n");
    }
    ini_time = clock();
    end_time = clock();
    elapsed_t = ((double) (end_time - ini_time)) / CLOCKS_PER_SEC;

    gettimeofday(&myTVstart, NULL);
    gettimeofday (&myTVend, NULL);
    elapsed_time = exec_time(&myTVstart, &myTVend);
    // creating the thread pool of workers
    workers = (threadpool*) malloc(sizeof(threadpool) * population_size);
    if(workers == NULL)
    {
        fprintf(stderr, "[ERR] unable to allocate threapool for parallel execution - abort\n");
        exit(EXIT_FAILURE);
    }
    for (a = 0; a < population_size; a++)
    {
        workers[a] = thpool_init(1);
    }
    //
    //
    // Evolving solution
    //for (g = 0; (g < generations) && (elapsed_t < max_time) && (steady_state < (int)(generations / population_size)); g++)
    //for (g = 0; (g < generations) && (elapsed_t < max_time) && (steady_state < generations); g++)
    for (g = 0; (g < generations) && (elapsed_t < max_time); g++)
    //for (g = 0; g < generations; g++)
    {
        // check if restart is required
        if (steady_state == (int)(generations / population_size))
        {
            steady_state = 0;
            fast_clone_agents(population_size, agent, *best_agent, population_size+1);
            fprintf(stdout, "[INF] restarting population\n");
        }
        //
        if (dbg == 2)
            fprintf(es, "[Iteration]: %d\n", g+1);
        // select the new parent (best agent)
        if (go_plot == true)
        {
            fprintf(es, "%6d  %.3lf\n", g, best_agent->efficiency);
        }
        //if (g > 0)
        if (g > 0)
        {
            a = get_new_parent(population_size, agent);
            fast_clone_agents(1, parent, agent[a], 2);
            if (dbg == 2)
            {
                // get new parent
                fprintf(es, "[parent]: (selected agent is %d)\n", a);
                fprintf(es, "  no. servers: %d\n", parent->no_servers);
                fprintf(es, "  no. tasks: %d\n", parent->no_tasks);
                fprintf(es, "  efficiency: %.3lf\n", parent->efficiency);
                fprintf(es, "  task-list: ");
                for (int t = 0; t < parent->no_tasks; t++)
                    fprintf(es, "%2d ", parent->task_list[t]);
                fprintf(es, "\n");
                fprintf(es, "  delta (vcpus): ");
                for (int n = 0; n < parent->no_servers; n++)
                    fprintf(es, "%2d ", parent->delta[n]);
                fprintf(es, "\n");
                fprintf(es, "  delta (mem): ");
                for (int n = 0; n < parent->no_servers; n++)
                    fprintf(es, "%2d ", parent->deltaM[n]);
                fprintf(es, "\n");
            }
        }
        //
        //
        // check if we have a new best agent
        if (parent->efficiency < best_agent->efficiency)
        {
            fast_clone_agents(1, best_agent, (*parent), 2);
            //consolidate_agent(best_agent, task_t, 0, max_load);
            //check_schedule(best_agent);
            //best_agent->efficiency = agent_efficiency(0, best_agent, max_load, max_power_dc);
            //eff_tmp = accounting_migration(fresh_load, best_agent, 0, max_power_dc, max_load, task_t);
            //best_agent->efficiency += eff_tmp;
            //
            if (dbg == 2)
            {
                fprintf(es, "  ** found new best agent\n");
                fprintf(es, "  no. servers: %d\n", best_agent->no_servers);
                fprintf(es, "  no. tasks: %d\n", best_agent->no_tasks);
                fprintf(es, "  efficiency: %.3lf\n", best_agent->efficiency);
                fprintf(es, "  task-list: ");
                for (int t = 0; t < best_agent->no_tasks; t++)
                    fprintf(es, "%2d ", best_agent->task_list[t]);
                fprintf(es, "\n");
                fprintf(es, "  delta (vcpus): ");
                for (int n = 0; n < best_agent->no_servers; n++)
                    fprintf(es, "%2d ", best_agent->delta[n]);
                fprintf(es, "\n");
                fprintf(es, "  delta (mem): ");
                for (int n = 0; n < best_agent->no_servers; n++)
                    fprintf(es, "%2d ", best_agent->deltaM[n]);
                fprintf(es, "\n");
            }
            steady_state = 0;
        }
        else
        {
            steady_state++;
        }
        //
        // cloning the best agent
        fast_clone_agents(population_size, agent, *parent, a);
        if (dbg == 2)
        {
            fprintf(es, "  [cloning agents]:\n");
            for (int i = 0; i < population_size; i++) {
                fprintf(es, "  ** agent %d\n", i);
                fprintf(es, "  no. servers: %d\n", best_agent->no_servers);
                fprintf(es, "  no. tasks: %d\n", best_agent->no_tasks);
                fprintf(es, "  efficiency: %.3lf\n", best_agent->efficiency);
                fprintf(es, "  task-list: ");
                for (int t = 0; t < best_agent->no_tasks; t++)
                    fprintf(es, "%2d ", best_agent->task_list[t]);
                fprintf(es, "\n");
                fprintf(es, "  delta (vcpus): ");
                for (int n = 0; n < best_agent->no_servers; n++)
                    fprintf(es, "%2d ", best_agent->delta[n]);
                fprintf(es, "\n");
                fprintf(es, "  delta (mem): ");
                for (int n = 0; n < best_agent->no_servers; n++)
                    fprintf(es, "%2d ", best_agent->deltaM[n]);
                fprintf(es, "\n");
            }
        }
        //
        // try to parallelize the mutation of each agent
        //omp_set_num_threads(population_size);
        //#pragma omp parallel for
        //
        work.agent = agent;
        // mutating the new population
        for(a = 0; a < population_size; a++)
        {
            work.agent_id = a;
            op = rand() % 21;
            switch(op)
            {
                case 0:
                case 1: // this mutation try to move one task to a destination server (consolidation)
                        //agent_mutation_1(a, agent, task_t);
                        //thpool_add_work(workers[a], (void*)(agent_mutation_1), (void*) &work);
                        agent_mutation_1(a, agent, task_t, task_Mt);
                        break;

                case 2:
                case 3: // this mutation tries to select the best node (with the highest delta) where to consolidate the task
                        //thpool_add_work(workers[a], (void*)(agent_mutation_3), (void*) &work);
                        agent_mutation_3(a, agent, task_t, task_Mt);
                        break;

                case 4:
                case 5:
                case 6: // this operator consolidate a server (tries to load it as much as possible)
                        //thpool_add_work(workers[a], (void*)(agent_mutation_4), (void*) &work);
                        agent_mutation_4(a, agent, task_t, task_Mt);
                        break;

                case 7:
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17: // this operator swaps two tasks
                        //thpool_add_work(workers[a], (void*)(agent_mutation_2), (void*) &work);
                        agent_mutation_2(a, agent, task_t, task_Mt);
                        break;
                case 18:
                case 19:
                case 20: agent_mutation_5(a,agent, task_t,task_Mt);
                        break;
            }
        }
        // synchronize the thread pool
        //for (a = 0; a < population_size; a++)
        //{
        //    thpool_wait(workers[a]);
        //}
        //
        //
        // evaluate the population
        for (a = 0; a < population_size; a++)
        {
            agent[a].efficiency = agent_efficiency(a, agent, max_load, max_power_dc);
            // ********************
            eff_tmp = accounting_migration(fresh_load, agent, a, max_power_dc, max_load, task_t);
            agent[a].efficiency += eff_tmp;
            // ********************
            if(dbg)
            {
                fprintf(stderr, "[DBG] (agent = %d, iter = %d) migration power %3.lf\n", a, g, eff_tmp);
            }
        }
        if (dbg == 2)
            fprintf(es, "\n\n");
        end_time = clock();
        elapsed_t = ((double) (end_time - ini_time)) / CLOCKS_PER_SEC;
    }
    gettimeofday (&myTVend, NULL);
    elapsed_time = exec_time(&myTVstart, &myTVend);
    if (dbg)
        fprintf(es, "\n\n========= Stop =========\n");
    final_eff = best_agent->efficiency;

    // ********************
    //accounting_migration(fresh_load, best_agent, 0, max_power_dc, max_load, task_t);
    // ********************
    if (go_plot == true)
    {
        fprintf(es, "%6d  %.3lf\n", g, best_agent->efficiency);
    }
    fclose(es);
    //
    // Destroy the whole thread pool
    for (a = 0; a < population_size; a++)
    {
        thpool_destroy(workers[a]);
    }
    //
    //
    //
    // ======================================================
    // Optimization process completed
    // ======================================================
    // try to consolidate the best solution ever
    fprintf(stdout, "[INF] check best agent (without consolidation)\n");
    check_schedule(best_agent);
    fast_clone_agents(1, dummy, (*best_agent), 2);
    consolidate_agent(dummy, task_t, 0, max_load);
    fprintf(stdout, "[INF] check best agent (with consolidation)\n");
    check_schedule(dummy);
    dummy->efficiency = agent_efficiency(0, dummy, max_load, max_power_dc);
    eff_tmp = accounting_migration(fresh_load, dummy, 0, max_power_dc, max_load, task_t);
    dummy->efficiency += eff_tmp;
    if (dummy->efficiency < best_agent->efficiency)
    {
        fprintf(stdout, "[INF] consolidation has been effective\n");
        fast_clone_agents(1, best_agent, (*dummy), 2);
    }
    power_off(*best_agent, max_load, max_power_dc, max_mem);
    //best_agent->efficiency -= eff_tmp;
    //check_schedule(best_agent);
    summary = fopen("../data/summary.dat", "w");
    if (summary == NULL)
    {
        fprintf(stderr, "[ERR] opening summary output file\n");
        fprintf(stderr, "[ERR] discard report\n");
    }
    else
    {
        fprintf(summary, "no_tasks = %d\n", fresh_load.no_tasks);
        fprintf(summary, "no_servers = %d\n", fresh_load.no_servers);
        fprintf(summary, "cap value = %.2lf\n", cap);
        fprintf(summary, "Workload Schedule:\n");
        for (t = 0; t < no_tasks; t++)
        {
            if ((t > 0) && (t % 25 == 0))
                fprintf(summary, "\n");
            fprintf(summary, "%3d ", best_agent->task_list[t]);
        }
        fprintf(summary, "\n");
        fclose(summary);
    }
    if (dbg)
        fprintf(stdout, "\n\n======================================\n");
    fprintf(stdout, "[INF] initial efficiency = %.3lf KW\n", (init_eff / 1000));
    fprintf(stdout, "[INF] final efficiency = %.3lf KW\n", (final_eff / 1000));
    fprintf(stdout, "[INF] reduction factor = %.2lf\n", 1.00 - ((final_eff / 1000)/(init_eff / 1000)));
    fprintf(stdout, "[INF] elapsed (wall-clock) time = %.1lf sec.\n", elapsed_t);

    fprintf(stdout, "[TMR] elapsed (real) time = %3d.%6d sec. \n", elapsed_time->secs, elapsed_time->usecs);

    fprintf(stdout, "[INF] no. of tasks allocate (effective) = %d\n", best_agent->no_tasks);
    fprintf(stdout, "[INF] no. of iterations elapsed = %d\n", g);
    if (dbg)
        fprintf(stdout, "======================================\n");
    if (dbg == 2)
    {
        fprintf(stdout, "[Delta server]:\n");
        for (n = 0; n < no_servers; n++)
        {
            if ((n > 0) && (n % 25 == 0))
                fprintf(stdout, "\n");
            fprintf(stdout, "%3d ", best_agent->delta[n]);
        }
        fprintf(stdout, "\n");
    }
    fprintf(stdout, "[INF] freeing resources\n");
    fprintf(stdout, "[OUT] generating fitness (efficiency) plot script\n");
    //fitness_plot("./fitness_plot.gp", 0, (generations + 10), (final_eff - 100), (init_eff + 100));
    fitness_plot("./fitness_plot.gp", 0, (g + 25), (final_eff - 100), (init_eff + 100));


    // run the best fit
    if(false)
    {
    	schedule_BF(fresh_load.no_servers,
                fresh_load.no_tasks,
                no_task_t,
                no_server_t,
                cap_load,
                cap_mem,
                task_t,
                task_Mt,
                fresh_load.task_list,
                fresh_load.workload);
    }

    if (gen_workload == true)
    {
        //release_task_list(population_size, task_list);
        //release_workload(workload);
        //release_delta_server(population_size, delta_server);free(power_dc);
        free(delta);
        free(task_list);
        free(cap_load);
        free(fresh_load.delta);
        free(fresh_load.deltaM);
        free(fresh_load.task_list);
        free(fresh_load.workload);
        release_max_server(max_load);
        release_power_dc(power_dc);
        free(server_t);
        free(task_t);
        free(max_power_dc);
        free(deltaM);
        free(server_Mt);
        free(task_Mt);
        free(cap_mem);
        free(max_mem);
    }
    else
    {
        free(power_dc);
        free(cap_load);
        free(server_t);
        free(task_t);
        free(fresh_load.delta);
        free(fresh_load.deltaM);
        free(fresh_load.task_list);
        free(fresh_load.workload);
        release_max_server(max_load);
        free(max_power_dc);
        free(deltaM);
        free(server_Mt);
        free(task_Mt);
        free(cap_mem);
        free(max_mem);
        //release_power_dc(power_dc);
    }
    fprintf(stdout, "[END]\n\n");
    return SUCCESS;
}


// Function implementation
int poisson(double mean)
{
    // data
    int stop;
    int poi_value;
    double t_sum;

    // code
    // loop to generate Poisson values using exponential distribution
    poi_value = 0;
    t_sum = 0.0;
    stop = true;
    while(stop == true)
    {
        t_sum = t_sum + expon(mean);
        if (t_sum >= 1.0)
        {
            stop = false;
        }
        else
        {
            poi_value++;
        }
    }
    return (poi_value);
}


double randn(double mu, double sigma)
{
    // data
    double U1;
    double U2;
    double W;
    double mult;
    static double X1;
    static double X2;
    static int call = 0;

    // code
    if (call == 1)
    {
        call = !call;
        return (mu + sigma * (double) X2);
    }

    do
    {
        U1 = -1 + ((double) rand () / RAND_MAX) * 2;
        U2 = -1 + ((double) rand () / RAND_MAX) * 2;
        W = pow (U1, 2) + pow (U2, 2);
    }
    while (W >= 1 || W == 0);
    mult = sqrt ((-2 * log (W)) / W);
    X1 = U1 * mult;
    X2 = U2 * mult;
    call = !call;
    return (mu + sigma * (double) X1);
}


int pick_task(int task_no, double mu, double sigma)
{
    // data
    int t;
    int stop;
    double step;
    double sum;
    double p;
    double epsilon;

    // code
    epsilon = 1.0e-3;
    step = 1.0 / (double) task_no;
    //sum = 0.0;
    sum = step;
    p = randn(mu, sigma);
    stop = false;
    for (t = 0; t < task_no && stop == false; t++)
    {
        if ((sum + epsilon) >= p)
        {
            stop = true;
        }
        else
        {
            sum += step;
        }
    }
    // check correctness of selected class
    if (t < 0)
    {
        t = 0;
    }
    if (t == task_no)
    {
        t = task_no - 1;
    }
    return t;
}

double expon(double x)
{
    // data
    double z;
    double exp_value;

    // code
    // pull a uniform random number (0 < z < 1)
    do
    {
        z = (double)rand() / (double)RAND_MAX;
    }
    while ((z == 0) || (z == 1));
    //
    // compute exponential random variable using inversion method
    exp_value = -x * log(z);
    return(exp_value);
}


int check_schedule(struct fresh_load_s* agent)
{
    // data
    int n;
    int dummy_dbg;
    int pass;

    // code
    pass = true;
    for(n = 0; n < agent->no_servers && pass == true; n++)
    {
        if ((agent->delta[n] < 0) || (agent->deltaM[n] < 0))
        {
            fprintf(stdout, "[ERR] current schedule did not pass the check!\n");
            pass = false;
        }
    }
    dummy_dbg = dbg;
    dbg = true;
    if (pass == true && dbg)
    {
        fprintf(stdout, "[INF] current schedule passed the check\n");
    }
    dbg = dummy_dbg;
    return pass;
}


double power_off(struct fresh_load_s schedule, int* max_load, double* max_power_dc, int* max_mem)
{
    int n;
    int count;
    int dummy_dbg;
    double reduction;

    count = 0;
    reduction = 0.0;
    for (n = 0; n < schedule.no_servers; n++)
    {
        //if (schedule.delta[n] == max_load[n])
        if ((schedule.delta[n] == max_load[n]) && (schedule.deltaM[n] == max_mem[n]))
        {
            count++;
            reduction += max_power_dc[n] * FRAC_IDLE;
            if(dbg)
            {
                fprintf(stdout, "[INF] node %d can be switched off\n", n);
            }
        }
    }
    dummy_dbg = dbg;
    dbg = true;
    if (dbg)
    {
        fprintf(stdout, "[INF] up to %d nodes switched off\n", count);
    }
    dbg = dummy_dbg;
    return reduction;
}


double accounting_migration(struct fresh_load_s src, struct fresh_load_s* dst, int id, double* max_power, int* max_load, int* task_t)
{
    // data
    int t;
    double efficiency;
    double eff_t;

    // code
    efficiency = 0.0;
    if (escape == true)
        fprintf(stdout, "\n\n=========================\n");
        for (t = 0; t < dst[id].no_tasks; t++)
        {
            if (escape == true)
                fprintf(stdout, "task = %d (type = %d, src = %d, dst = %d): vcpu=%d tot_vcpu=%d max_pow=%.3lf  ",
                    t,
                    src.workload[t],
                    src.task_list[t],
                    dst[id].task_list[t],
                    task_t[src.workload[t]],
                    max_load[src.task_list[t]],
                    max_power[src.task_list[t]]);
            // check if the task has been migrated
            if (src.task_list[t] != dst[id].task_list[t])
            {
                eff_t = (double) task_t[src.workload[t]] / (double) max_load[src.task_list[t]];
                eff_t = eff_t * max_power[src.task_list[t]] * LAMBDA;
                efficiency += eff_t;
                if (escape == true)
                    fprintf(stdout, " => mig = %3lf  tot_mig = %3lf\n", eff_t, efficiency);
            }
            else
            {
                if (escape == true)
                    fprintf(stdout, "=> none\n");
            }
        }
    //    escape++;
    //}
    return efficiency;
}



double agent_efficiency(int agent_id, struct fresh_load_s* agent, int* max_load, double* max_power_dc)
{
    // data
    int n;
    int cap;
    double frac;
    double efficiency;
    double p_idle;

    // code
    efficiency = 0.0;
    for (n = 0; n < agent[agent_id].no_servers; n++)
    {
        // added to take into account the amount of power draw in idle mode
        p_idle = max_power_dc[n] * FRAC_IDLE;
        // compute the fraction of utilization
        cap = max_load[n] - agent[agent_id].delta[n];
        // check if the machine is not used => mark it as to shutdown (power consumption is set to 0)
        if (cap > 0)
        {
            frac = (double) cap / (double) max_load[n];
            // get the actual efficiency
            efficiency += ((max_power_dc[n] - p_idle) * frac) + p_idle;
        }
    }
    return efficiency;
}



struct time_diff_s* exec_time(struct timeval* start, struct timeval* end)
{
    // data
    struct time_diff_s* diff;
    //  code
    diff = (struct time_diff_s*) malloc (sizeof (struct time_diff_s));

    if (start->tv_sec == end->tv_sec) {
        diff->secs = 0;
        diff->usecs = end->tv_usec - start->tv_usec;
    }
    else {
        diff->usecs = 1000000 - start->tv_usec;
        diff->secs = end->tv_sec - (start->tv_sec + 1);
        diff->usecs += end->tv_usec;
        if (diff->usecs >= 1000000) {
            diff->usecs -= 1000000;
            diff->secs += 1;
        }
    }
    return diff;
}


int consolidate_agent(struct fresh_load_s* agent, int* task_t, int agent_id, int* max_load)
{
    // data
    int n;
    int t;
    int load;
    int task_1;
    int task_2;
    int src;
    int dst;
    int delta;
    int go;
    int id;
    int k;

    //int stop;

    // code
    id = agent_id;
    if (dbg)
    {
        fprintf(stdout, "[INF] starting consolidation phase\n");
    }
    for (n = 0; n < agent[id].no_servers; n++)
    {
            go = true;
            //stop = false;
            while(go == true)
            {
                go = false;
                load = 0;
                for (t = 0; t < agent[id].no_tasks; t++)
                {
                    task_1 = task_t[agent[id].workload[t]];
                    task_2 = task_t[agent[id].workload[load]];
                    delta = agent[id].delta[n];
                    if ((task_1 > task_2) && (task_1 <= delta) && (agent[id].task_list[t] != n) &&
                        (delta < max_load[n]) && (delta > 0))
                    {
                        load = t;
                        go = true;
                        //stop = true;
                    }
                }
                if (go == true)
                {
                    if (dbg)
                    {
                        fprintf(stdout, "[DBG] consolidate node_%d (moving task %d):\n", n, load);
                    }
                    dst = n;
                    src = agent[id].task_list[load];
                    if (dbg)
                    {
                        fprintf(stdout, "     src_node = %d\n", src);
                        fprintf(stdout, "     dst_node = %d\n", dst);
                    }
                    task_1 = task_t[agent[id].workload[load]];
                    if (dbg)
                    {
                        fprintf(stdout, "     task_load = %d\n", task_1);
                        fprintf(stdout, "     delta[src] = %d\n", agent[id].delta[src]);
                        fprintf(stdout, "     delta[dst] = %d\n", agent[id].delta[dst]);
                    }
                    agent[id].delta[src] += task_1;
                    agent[id].delta[dst] -= task_1;
                    agent[id].task_list[load] = dst;
                    if (dbg)
                    {
                        fprintf(stdout, "     task assigned to = %d\n", agent[id].task_list[load]);
                        fprintf(stdout, "     delta[src] = %d\n", agent[id].delta[src]);
                        fprintf(stdout, "     delta[dst] = %d\n", agent[id].delta[dst]);
                    }
                }
            }
            if (dbg)
            {
                fprintf(stdout, "[DBG] delta vector updated:\n");
                for (k = 0; k < agent[id].no_servers; k++) {
                    if (k % 25 == 0)
                        fprintf(stdout, "\n");
                    fprintf(stdout, "%3d ", agent[id].delta[k]);
                }
                fprintf(stdout, "\n");
            }//if (stop == true)
            //    break;
    }
    if (dbg)
    {
        fprintf(stdout, "[INF] consolidation phase completed\n");
    }
    return SUCCESS;
}



// this operator tries, given a server,  to spread all its workload on other machines
int agent_mutation_5(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt)
{
    // data
    int n;
    int s;
    int t;
    int id;
    int go;
    int t_count;
    int bool_1;
    int bool_2;
    int bool_3;
    int* delta;
    int* deltaM;
    int* t_list;
    double pm;

    // code
    id = agent_id;
    delta = (int*) malloc(sizeof(int) * agent[id].no_servers);
    deltaM = (int*) malloc(sizeof(int) * agent[id].no_servers);
    t_list = (int*) malloc(sizeof(int) * agent[id].no_tasks);
    if (delta == NULL || deltaM == NULL || t_list == NULL)
    {
        if (dbg)
        {
            fprintf(stdout, "[ERR] unable to allocate memory for operator type 5\n");
        }
        return FAILURE;
    }
    // create a copy of deltas and allocation for rollback
    for (n = 0; n < agent[id].no_servers; n++)
    {
        delta[n] = agent[id].delta[n];
        deltaM[n] = agent[id].deltaM[n];
    }
    for (t = 0; t < agent[id].no_tasks; t++)
    {
        t_list[t] = agent[id].task_list[t];
    }
    // perform mutation
    pm = 0.0;
    for (n = 0; n < agent[id].no_servers; n++)
    {
        go = 0;
        pm = (double)rand() / (double)RAND_MAX;
        if (pm < mu_5)
        {
            t_count = 0;
            for (t = 0; t < agent[id].no_tasks; t++)
            {
                if (agent[id].task_list[t] == n)
                {
                    t_count++;
                    for (s = 0; s < agent[id].no_servers; s++)
                    {
                        bool_1 = s != n;
                        bool_2 = delta[s] >= task_t[agent[id].workload[t]];
                        bool_3 = deltaM[s] >= task_Mt[agent[id].workload[t]];
                        if (bool_1 && bool_2 && bool_3)
                        {
                            go++;
                            delta[s] -= task_t[agent[id].workload[t]];
                            deltaM[s] -= task_Mt[agent[id].workload[t]];
                            delta[n] += task_t[agent[id].workload[t]];
                            deltaM[n] += task_Mt[agent[id].workload[t]];
                            t_list[t] = s;
                        }
                    }
                }
            }
            // check if make the swap for the selected server
            if (t_count == go)
            {
                for (s = 0; s < agent[id].no_servers; s++)
                {
                    agent[id].delta[s] = delta[s];
                    agent[id].deltaM[s] = deltaM[s];
                }
                for (t = 0; t < agent[id].no_tasks; t++)
                {
                    agent[id].task_list[t] = t_list[t];
                }
            }
            else
            {
                // rollback to the previous solution
                for (n = 0; n < agent[id].no_servers; n++)
                {
                    delta[n] = agent[id].delta[n];
                    deltaM[n] = agent[id].deltaM[n];
                }
                for (t = 0; t < agent[id].no_tasks; t++)
                {
                    t_list[t] = agent[id].task_list[t];
                }
            }
        }
    }
    // free resources
    free(delta);
    free(deltaM);
    free(t_list);
    return SUCCESS;
}




// this operator consolidate a server (tries to load it as much as possible)
//void agent_mutation_4(void* w)
int agent_mutation_4(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt)
{
    // data
    int n;
    int t;
    int load;
    int task_1;
    int task_2;
    int task_M1;
    int task_M2;
    int src;
    int dst;
    int delta;
    int deltaM;
    int go;
    int id;
    int bool_1;
    int bool_2;
    int bool_3;
    int bool_4;
    int bool_5;
    double pm;

    // code
    id = agent_id;
    pm = 0.0;
    for (n = 0; n < agent[id].no_servers; n++)
    {
        pm = (double)rand() / (double)RAND_MAX;
        if (pm < mu_4)
        {
            //fprintf(stdout, "[DBG] consolidate\n");
            go = true;
            while (go == true)
            {
                go = false;
                load = 0;
                for (t = 0; t < agent[id].no_tasks; t++)
                {
                    task_1 = task_t[agent[id].workload[t]];
                    task_M1 = task_Mt[agent[id].workload[t]];
                    task_2 = task_t[agent[id].workload[load]];
                    task_M2 = task_Mt[agent[id].workload[load]];
                    delta = agent[id].delta[n];
                    deltaM = agent[id].deltaM[n];
                    bool_1 = task_1 > task_2;
                    bool_2 = task_M1 > task_M2;
                    bool_3 = task_1 <= delta;
                    bool_4 = task_M1 <= deltaM;
                    bool_5 = agent[id].task_list[t] != n;
                    //if ((task_1 > task_2) && (task_1 <= delta) && (agent[id].task_list[t] != n))
                    if ((bool_1 || bool_2) && bool_3 && bool_4 && bool_5)
                    {
                        load = t;
                        go = true;
                    }
                }
                if (go == true)
                {
                    dst = n;
                    src = agent[id].task_list[load];
                    task_1 = task_t[agent[id].workload[load]];
                    task_M1 = task_Mt[agent[id].workload[load]];
                    agent[id].delta[src] += task_1;
                    agent[id].delta[dst] -= task_1;
                    agent[id].deltaM[src] += task_M1;
                    agent[id].deltaM[dst] -= task_M1;
                    agent[id].task_list[load] = dst;
                }
            }
        }
    }
    return SUCCESS;
}


// this mutation tries to select the best node (with the highest delta) where to consolidate the task
//void agent_mutation_3(void* w)
int agent_mutation_3(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt)
{
    // data
    int t;
    int n;
    int mig;
    int id;
    int bool_1;
    int bool_2;
    int bool_3;
    int bool_4;
    int max_delta;
    int max_deltaM;
    double pm;
    FILE* fp;

    // code
    id = agent_id;
    pm = 0.0;
    for (t = 0; t < agent[id].no_tasks; t++)
    {
        mig = -1;
        pm = (double)rand() / (double)RAND_MAX;
        if (pm < mu_3)
        {
            max_delta = 0;
            max_deltaM = 0;
            for (n = 0; n < agent[id].no_servers; n++)
            {
                if (n == agent[id].task_list[t])
                {
                    n++;
                }
                // if ((agent[id].delta[n] >= task_t[agent[id].workload[t]]) &&
                //     (agent[id].delta[n] > max_delta))
                bool_1 = agent[id].delta[n] >= task_t[agent[id].workload[t]];
                bool_2 = agent[id].deltaM[n] >= task_Mt[agent[id].workload[t]];
                bool_3 = agent[id].delta[n] > max_delta;
                bool_4 = agent[id].deltaM[n] > max_deltaM;
                if (bool_1 && bool_2 && (bool_3 || bool_4))
                {
                    max_delta = agent[id].delta[n];
                    max_deltaM = agent[id].deltaM[n];
                    mig = n;
                }
            }
            if (mig >= 0)
            {
                agent[id].delta[mig] -= task_t[agent[id].workload[t]];
                agent[id].deltaM[mig] -= task_Mt[agent[id].workload[t]];
                agent[id].delta[agent[id].task_list[t]] += task_t[agent[id].workload[t]];
                agent[id].deltaM[agent[id].task_list[t]] += task_Mt[agent[id].workload[t]];
                agent[id].task_list[t] = mig;
            }
        }
    }
    //return;
    return SUCCESS;
}


// this operator tries to swap two tasks each other
//void agent_mutation_2(void* w)
int agent_mutation_2(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt)
{
    // data
    int t;
    int j;
    int n;
    int go;
    int id;
    int node_1;
    int node_2;
    int t_delta_1;
    int t_delta_2;
    int n_delta_1;
    int n_delta_2;
    int t_deltaM_1;
    int t_deltaM_2;
    int n_deltaM_1;
    int n_deltaM_2;
    int bool_1;
    int bool_2;
    int bool_3;
    int bool_4;
    int bool_5;
    double pm;
    FILE* fp;

    // code
    id = agent_id;
    pm = 0.0;
    for (t = 0; t < agent[id].no_tasks; t++)
    {
        pm = (double)rand() / (double)RAND_MAX;
        if (pm < mu_2)
        {
            go = true;
            t_delta_1 = task_t[agent[id].workload[t]];
            t_deltaM_1 = task_Mt[agent[id].workload[t]];
            node_1 = agent[id].task_list[t];
            for (j = 0; j < agent[id].no_tasks && go; j++)
            {
                t_delta_2 = task_t[agent[id].workload[j]];
                t_deltaM_2 = task_Mt[agent[id].workload[j]];
                node_2 = agent[id].task_list[j];
                n_delta_1 = (agent[id].delta[node_1] - t_delta_1) + t_delta_2;
                n_delta_2 = (agent[id].delta[node_2] - t_delta_2) + t_delta_1;
                n_deltaM_1 = (agent[id].deltaM[node_1] - t_deltaM_1) + t_deltaM_2;
                n_deltaM_2 = (agent[id].deltaM[node_2] - t_deltaM_2) + t_deltaM_1;
                bool_1 = j!= t;
                bool_2 = n_delta_1 >= 0;
                bool_3 = n_delta_2 >= 0;
                bool_4 = n_deltaM_1 >= 0;
                bool_5 = n_deltaM_2 >= 0;
                if (bool_1 && bool_2 && bool_3 && bool_4 && bool_5)
                {
                    // it is possible to swap the two tasks
                    go = false;
                    agent[id].delta[node_1] = n_delta_1;
                    agent[id].delta[node_2] = n_delta_2;
                    agent[id].deltaM[node_1] = n_deltaM_1;
                    agent[id].deltaM[node_2] = n_deltaM_2;
                    agent[id].task_list[t] = node_2;
                    agent[id].task_list[j] = node_1;
                }
            }
        }
    }
    //return;
    return SUCCESS;
}


// this mutation try to move one task to a destination server (consolidation)
//void agent_mutation_1(void* w)
int agent_mutation_1(int agent_id, struct fresh_load_s* agent, int* task_t, int* task_Mt)
{
    // data
    int t;
    int n;
    int go;
    double pm;
    int id;
    //struct work_s* work;
    FILE* fp;

    // code
    //work = (struct work_s*) w;
    id = agent_id;
    if (dbg)
    {
        fprintf(es, "  [mutation]: agent %d\n", id);
        fprintf(es, "  (ini) delta (vcpus): ");
        for (int i = 0; i < agent[id].no_servers; i++)
        {
            if (i % 25 == 0)
                fprintf(stdout, "\n");
            fprintf(es, "%2d ", agent[id].delta[i]);
        }
        fprintf(es, "\n");
        fprintf(es, "  (ini) delta (mem): ");
        for (int i = 0; i < agent[id].no_servers; i++)
        {
            if (i % 25 == 0)
                fprintf(stdout, "\n");
            fprintf(es, "%2d ", agent[id].deltaM[i]);
        }
        fprintf(es, "\n");
    }
    pm = 0.0;
    for (t = 0; t < agent[id].no_tasks; t++)
    {
        go = true;
        pm = (double)rand() / (double)RAND_MAX;
        if (pm < mu_1)
        {
            if (dbg)
            {
                fprintf(es, "    mutate task %d (on node %d) ==> ", t, agent[id].task_list[t]);
            }
            for (n = 0; n < agent[id].no_servers && go; n++)
            {
                if (n == agent[id].task_list[t]) {
                    n++;
                }
                //--if ((agent[agent_id].delta[n] >= task_t[agent[agent_id].workload[t]]) && (agent[agent_id].delta[n] > max_delta))
                //if (agent[id].delta[n] >= task_t[agent[id].workload[t]])
                if ((agent[id].delta[n] >= task_t[agent[id].workload[t]]) &&
                        (agent[id].deltaM[n]) >= task_Mt[agent[id].workload[t]])
                {
                    agent[id].delta[n] -= task_t[agent[id].workload[t]];
                    agent[id].deltaM[n] -= task_Mt[agent[id].workload[t]];
                    // update the other delta node that must be incremented
                    agent[id].delta[agent[id].task_list[t]] += task_t[agent[id].workload[t]];
                    agent[id].deltaM[agent[id].task_list[t]] += task_Mt[agent[id].workload[t]];
                    //
                    agent[id].task_list[t] = n;
                    if (dbg)
                    {
                        fprintf(es, "move to node %d\n", n);
                    }
                    go = false;
                }
            }
            if (go == true)
            {
                if (dbg)
                {
                    fprintf(es, "++ not possible to move ++\n");
                }
            }
        }
    }
    if (dbg)
    {
        fprintf(es, "  (end) delta (vcpus): ");
        for (int i = 0; i < agent[id].no_servers; i++)
        {
            if (i % 25 == 0)
                fprintf(stdout, "\n");
            fprintf(es, "%2d ", agent[id].delta[i]);
        }
        fprintf(es, "\n");
        fprintf(es, "  (end) delta (mem): ");
        for (int i = 0; i < agent[id].no_servers; i++)
        {
            if (i % 25 == 0)
                fprintf(stdout, "\n");
            fprintf(es, "%2d ", agent[id].deltaM[i]);
        }
        fprintf(es, "\n");
    }
    //return;
    return SUCCESS;
}


int fast_clone_agents(int population_size, struct fresh_load_s* agent, struct fresh_load_s parent, int skip_agent)
{
    // data
    int a;
    int t;
    int n;


    // code
    for (a = 0; a < population_size; a++)
    {
        if (a != skip_agent)
        {
            // cloning the task_list and workload
            for (t = 0; t < parent.no_tasks; t++)
            {
                agent[a].task_list[t] = parent.task_list[t];
                agent[a].workload[t] = parent.workload[t];
            }
            // cloning the delta vector
            for (n = 0; n < parent.no_servers; n++)
            {
                agent[a].delta[n] = parent.delta[n];
                agent[a].deltaM[n] = parent.deltaM[n];
            }
            agent[a].efficiency = parent.efficiency;
        }
    }
    return SUCCESS;
}


int get_new_parent(int population_size, struct fresh_load_s* agent)
{
    // data
    int best_agent;
    int a;

    // code
    best_agent = 0;
    for (a = 1; a < population_size; a++)
    {
        if (agent[a].efficiency < agent[best_agent].efficiency)
            best_agent = a;
    }
    return best_agent;
}


int dump_agents(struct fresh_load_s* agent, int population_size)
{
    // data
    int a;
    int n;
    int t;
    FILE* fp;

    // code
    fprintf(stdout, "[DBG] DUMPING AGENTS\n");
    fp = fopen("../data/allocation.dbg", "a");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] error opening dump file (../data/allocation.dbg)\n");
        return FAILURE;
    }
    fprintf(fp,  "\n** DUMP AGENT POPULATION **\n");
    for (a = 0; a < population_size; a++)
    {
        fprintf(fp, "***********************\n* dump of agent %d:\n", a);
        fprintf(fp, "  workload:\n");
        for (t = 0; t < agent[a].no_tasks; t++)
        {
            if ((t != 0) && (t % 50 == 0))
                fprintf(fp, "\n");
            fprintf(fp, "%3d ", agent[a].workload[t]);
        }
        fprintf(fp, "\n  task-list:\n");
        for (t = 0; t < agent[a].no_tasks; t++)
        {
            if ((t != 0) && (t % 50 == 0))
                fprintf(fp, "\n");
            fprintf(fp, "%3d ", agent[a].task_list[t]);
        }
        fprintf(fp, "\n  delta-server (VCPUs):\n");
        for (n = 0; n < agent[a].no_servers; n++)
        {
            if ((n != 0) && (n % 50 == 0))
                fprintf(fp, "\n");
            fprintf(fp, "%3d ", agent[a].delta[n]);
        }
        fprintf(fp, "\n  delta-server (MEM):\n");
        for (n = 0; n < agent[a].no_servers; n++)
        {
            if ((n != 0) && (n % 50 == 0))
                fprintf(fp, "\n");
            fprintf(fp, "%3d ", agent[a].deltaM[n]);
        }
        fprintf(fp, "\n  efficiency:\n");
        fprintf(fp, "%.3lf\n", agent[a].efficiency);
    }
    fclose(fp);
    return SUCCESS;
}



void header_msg(void)
{
    // data
    char* msg = "       _                 _   _               _       \n"
                "   ___| | ___  _   _  __| | | |__  _ __ __ _(_)_ __  \n"
                "  / __| |/ _ \\| | | |/ _` | | '_ \\| '__/ _` | | '_ \\ \n"
                " | (__| | (_) | |_| | (_| | | |_) | | | (_| | | | | |\n"
                "  \\___|_|\\___/ \\__,_|\\__,_| |_.__/|_|  \\__,_|_|_| |_|\n"
                "                                                     ";

    // code
    fprintf(stdout, "%s", msg);
    fprintf(stdout, "\n\nEvolutionary Strategy For Server Workload Optimization\n\n");
    return;
}



struct fresh_load_s* clone_agents(int population_size, struct fresh_load_s fresh_load)
{
    // data
    int a;
    int t;
    int n;
    struct fresh_load_s* agent;

    // code
   agent = NULL;
   agent = (struct fresh_load_s*) malloc(sizeof(struct fresh_load_s) * population_size);
   if (agent == NULL)
   {
       fprintf(stderr, "[ERR] memory allocation error (agents' population)\n");
       return (NULL);
   }
   for (a = 0; a < population_size; a++)
   {
       agent[a].efficiency = fresh_load.efficiency;
       agent[a].no_servers = fresh_load.no_servers;
       agent[a].no_tasks = fresh_load.no_tasks;
       agent[a].workload = (int*) malloc(sizeof(int) * agent[a].no_tasks);
       agent[a].task_list = (int*) malloc(sizeof(int) * agent[a].no_tasks);
       agent[a].delta = (int*) malloc(sizeof(int) * fresh_load.no_servers);
       agent[a].deltaM = (int*) malloc(sizeof(int) * fresh_load.no_servers);
       if (agent[a].workload == NULL || agent[a].task_list == NULL || agent[a].delta == NULL || agent[a].deltaM == NULL)
       {
           fprintf(stderr, "[ERR] memory allocation error (agent %d)\n", a);
           free(agent);
           return NULL;
       }
       // cloning agent
       for (t = 0; t < agent[a].no_tasks; t++)
       {
           agent[a].task_list[t] = fresh_load.task_list[t];
           agent[a].workload[t] = fresh_load.workload[t];
       }
       for (n = 0; n < agent[a].no_servers; n++)
       {
           agent[a].delta[n] = fresh_load.delta[n];
           agent[a].deltaM[n] = fresh_load.deltaM[n];
       }
   }
   return agent;
}


double* get_power_dc(int no_servers, int* max_load, int* cap_load, double* max_power)
{
    // data
    int n;
    double frac;
    double* power_dc;
    double p_idle;

    // code
    power_dc = (double*) malloc(sizeof(double) * no_servers);
    if (power_dc == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error\n");
        return NULL;
    }
    if (dbg)
        fprintf(stdout, "[DBG] DATA CENTER POWER CONSUMPTION\n");
    for (n = 0; n < no_servers; n++)
    {
        // take into account idle power and servers that can be switched off if in idle
        p_idle = max_power[n] * FRAC_IDLE;
        if (cap_load[n] > 0)
        {
            frac = (double) cap_load[n] / (double) max_load[n];
            // power_dc[n] = max_power[n] * frac;
            power_dc[n] = ((max_power[n] - p_idle) * frac) + p_idle;
        }
        else
        {
            // server not used are marked as switched off so no power consumption from them
            power_dc[n] = 0.0;
        }
        if (dbg)
            fprintf(stdout, "Node %2d = %lf W (fraction %lf)\n", n, power_dc[n], frac);
    }
    return power_dc;
}


double summary_power_dc(double* power_vector, int no_servers)
{
    // data
    double total_power_dc;
    int n;

    // code
    for (n = 0; n < no_servers; n++)
    {
        total_power_dc += power_vector[n];
    }
    return total_power_dc;
}


struct fresh_load_s get_fresh_load(int* max_load,
                                   int* cap_load,
                                   int no_servers,
                                   int* task_t,
                                   int no_task_t,
                                   int no_tasks,
                                   int* max_mem,
                                   int* cap_mem,
                                   int* task_Mt)
{
    // data
    int t;
    int dummy_dbg;
    int n;
    int rnd;
    int scan;
    int go;
    int task;
    int taskM;
    int mx_distr;
    int capped_node;
    int capped_mem;
    double std_dev;
    double std_devM;
    double frac;
    double fracM;
    double tot_fracM;
    double tot_frac;
    struct fresh_load_s fresh_load;
    FILE* fp;
    FILE* wl;
    FILE* tl;

    // code
    fp = fopen("../data/allocation.dbg", "w");
    // initialize structure
    fresh_load.no_tasks = 0;
    fresh_load.task_list = (int*) malloc(sizeof(int) * no_tasks);
    if (dbg)
        fprintf(fp, "* allocating data structures\n");
    if (fresh_load.task_list == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error (fresh_load.task_list)\n");
        fresh_load.workload = NULL;
        return fresh_load;
    }
    fresh_load.workload = (int*) malloc(sizeof(int) * no_tasks);
    if (fresh_load.task_list == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error (fresh_load.workload");
        free(fresh_load.task_list);
        return fresh_load;
    }
    // initialization of data structures
    for (t = 0; t < no_tasks; t++)
    {
        fresh_load.workload[t] = -1;
        fresh_load.task_list[t] = -1;
    }
    if (dbg)
    {
        fprintf(fp, "* workload initialization:\n");
        for (t = 0; t < no_tasks; t++) {
            if ((t != 0) && (t % 50 == 0))
                fprintf(fp, "\n");
            fprintf(fp, "%2d ", fresh_load.workload[t]);
        }
        fprintf(fp, "\n* task_list initialization:\n");
        for (t = 0; t < no_tasks; t++) {
            if ((t != 0) && (t % 50 == 0))
                fprintf(fp, "\n");
            fprintf(fp, "%2d ", fresh_load.task_list[t]);
        }
    }
    // try to generate the workload
    if (dbg)
        fprintf(fp, "\n* generate a fresh workload:\n");
    go = true;
    t = 0;
    mx_distr = 0;
    for (n = 0; (n < no_servers) && go; n++)
    {
        if (dbg)
            fprintf(fp, "  generate load for node %d\n", n);
        capped_node = 0;
        capped_mem = 0;
        scan = -1;
        while ((capped_node < cap_load[n]) && (capped_mem < cap_mem[n]))
        {
            if (scan < 0)
            {
                if (rand_type == UNIFORM)
                {
                    rnd = rand() % no_task_t;
                }
                else if (rand_type == NORMAL)
                {
                    rnd = pick_task(no_task_t, 0.0, 0.69);
                }
                else if (rand_type == MIXED)
                {
                    if (mx_distr % 5 == 0)
                        rnd = pick_task(no_task_t, 1.0, 2.15);
                    else
                        rnd = rand() % no_task_t;
                }
                else
                {
                    rnd = 0;
                }
                task = task_t[rnd];
                taskM = task_Mt[rnd];
                if (dbg)
                {
                    fprintf(fp, "  try with this initial task type %d (vcpu = %d)\n", rnd, task);
                    fprintf(fp, "  try with this initial task type %d (mem = %d)\n", rnd, taskM);
                }
            }
            else
            {
                task = task_t[scan];
                taskM = task_Mt[scan];
                if (scan == rnd)
                    break;
                if (dbg)
                {
                    fprintf(fp, "  try with task type %d (vcpu = %d)\n", scan, task);
                    fprintf(fp, "  try with task type %d (mem = %d)\n", scan, taskM);
                }
            }
            if ((((capped_node + task) <= cap_load[n]) || ((capped_node + task) < max_load[n])) &&
                    (((capped_mem + taskM) <= cap_mem[n]) || ((capped_mem + taskM) < max_mem[n])))
            {
                if (dbg)
                    fprintf(fp, "  task found (cur_cap = %d  eff_cap = %d  max_cap = %d])\n", capped_node, cap_load[n], max_load[n]);
                capped_node += task;
                capped_mem += taskM;
                if (scan < 0)
                    fresh_load.workload[t] = rnd;
                else
                    fresh_load.workload[t] = scan;
                fresh_load.task_list[t] = n;
                if (dbg)
                    fprintf(fp, "  task allocated on node = %d (type = %d)\n", fresh_load.task_list[t], fresh_load.workload[t]);
                t++;
                scan = -1;
                if (t == no_tasks)
                {
                    go = false;
                    break;
                }
            }
            else
            {
                if (scan < 0)
                    scan = rnd + 1;
                else
                    scan = scan + 1;
                if (scan == no_task_t)
                    scan = 0;
                if (dbg)
                    fprintf(fp, "  move on task (type=%d)\n", scan);
            }
        }
    }
    fresh_load.no_tasks = t;
    dummy_dbg = dbg;
    dbg = true;
    if (dbg == true)
    {
        fprintf(fp, "* allocated %d tasks on %d nodes\n\n", t, n);
        fprintf(stdout, "[DBG] allocated %d tasks on %d nodes\n", t, n);
    }
    // final check -- print allocated vectors;
    dbg = dummy_dbg;
    if (dbg)
    {
        fprintf(fp, "* ==========================\n* task allocation:\n* ==========================\n");
        for (t = 0; t < no_tasks; t++)
        {
            if (fresh_load.task_list[t] != -1)
            {
                fprintf(fp, "[task %3d]: node_%3d (type: %2d - VCPU: %2d - MEM: %2d) --- ", t, fresh_load.task_list[t],
                        fresh_load.workload[t], task_t[fresh_load.workload[t]], task_Mt[fresh_load.workload[t]]);
                n = fresh_load.task_list[t];
                fprintf(fp, "[node %3d]: eff_cap=%2d  max_cap=%2d  max_mem=%2d  eff_mem=%2d\n", n, cap_load[n], max_load[n],
                        max_mem[n], cap_mem[n]);
            }
        }
    }
    // generate files associated to the workload generation
    wl = fopen("../data/workload.dat", "w");
    tl = fopen("../data/task_list.dat", "w");
    if ((wl == NULL) || (tl == NULL))
    {
        fprintf(stderr, "[ERR] error opening output file (workload.dat/task_list.dat) - not possible to save the workload\n");
        fclose(wl);
        fclose(tl);
    }
    else
    {
        for (t = 0; t < fresh_load.no_tasks; t++)
        {
            fprintf(wl, "%d\n", fresh_load.workload[t]);
            fprintf(tl, "%d\n", fresh_load.task_list[t]);
        }
        fclose(wl);
        fclose(tl);
    }
    // adjust the cap value
    if (dbg)
        fprintf(fp, "** Updated server capping:\n");
    tot_frac = 0.0;
    tot_fracM = 0.0;
    for (n = 0; n < no_servers; n++)
    {
        capped_node = 0;
        capped_mem = 0;
        for (t = 0; t < fresh_load.no_tasks; t++)
        {
            if (fresh_load.task_list[t] == n)
            {
                capped_node += task_t[fresh_load.workload[t]];
                capped_mem += task_Mt[fresh_load.workload[t]];
            }
        }
        cap_load[n] = capped_node;
        cap_mem[n] = capped_mem;
        frac = (double) cap_load[n] / (double) max_load[n];
        tot_frac += frac;
        fracM = (double) cap_mem[n] / (double) max_mem[n];
        tot_fracM += fracM;
        fprintf(fp, " Node %2d:  VCPU=%d (fractional %.3lf) || MEM=%d (fractional%.3lf)\n", n, cap_load[n], frac,
                cap_mem[n], fracM);
    }
    tot_frac = tot_frac / (double) no_servers;
    tot_fracM = tot_fracM / (double) no_servers;
    std_dev = 0.0;
    std_devM = 0.0;
    for (n = 0; n < no_servers; n++)
    {
        frac = (double) cap_load[n] / (double) max_load[n];
        std_dev += ((frac - tot_frac) * (frac - tot_frac));
        fracM = (double) cap_mem[n] / (double) max_mem[n];
        std_devM += ((fracM - tot_fracM) * (fracM - tot_fracM));
    }
    std_dev = std_dev / (double) (no_servers - 1);
    std_dev = sqrt(std_dev);
    std_devM = std_devM / (double) (no_servers - 1);
    std_devM = sqrt(std_devM);
    dummy_dbg = dbg;
    dbg = true;
    if (dbg)
    {
        fprintf(fp, "** STATISTICS WORKLOAD:\n");
        fprintf(fp, "** mean fractional allocation (VCPUs) = %.3lf\n", tot_frac);
        fprintf(fp, "** standard deviation (VCPUs)= %.3lf\n", std_dev);
        fprintf(fp, "** mean fractional allocation (MEM) = %.3lf\n", tot_fracM);
        fprintf(fp, "** standard deviation (MEM)= %.3lf\n", std_devM);
        fprintf(stdout, "[INF] STATISTICS WORKLOAD:\n");
        fprintf(stdout, "[INF] mean fractional allocation (VCPUs) = %.3lf\n", tot_frac);
        fprintf(stdout, "[INF] standard deviation (VCPUs)= %.3lf\n", std_dev);
        fprintf(stdout, "[INF] mean fractional allocation (MEM) = %.3lf\n", tot_fracM);
        fprintf(stdout, "[INF] standard deviation (MEM)= %.3lf\n", std_devM);
    }
    dbg = dummy_dbg;
    fclose(fp);
    if (dbg)
        fprintf(stdout, "[INF] calculating delta resources\n");
    fresh_load.delta = (int*) malloc(sizeof(int) * no_servers);
    fresh_load.deltaM = (int*) malloc(sizeof(int) * no_servers);
    if (fresh_load.delta == NULL || fresh_load.deltaM == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error (delta or deltaM)\n");
    }
    else
    {
        dummy_dbg = false;
        for (n = 0; n < no_servers; n++)
        {
            if (n % 50 == 0 && dummy_dbg)
                fprintf(stdout, "\n");
            fresh_load.delta[n] = max_load[n] - cap_load[n];
            fresh_load.deltaM[n] = max_mem[n] - cap_mem[n];
            if (dummy_dbg == true)
            {
                fprintf(stdout, "%2d ", fresh_load.deltaM[n]);
            }
        }
        if (dummy_dbg == true)
            fprintf(stdout, "\n");
    }
    return fresh_load;
}


// provides the maxmimum amount of server memory to use during task allocation
int* get_cap_memory(int no_servers, int* max_mem)
{
    //data
    int n;
    int* cap_mem;

    // code
    cap_mem = (int*) malloc(sizeof(int) * no_servers);
    if (cap_mem == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation (cap_load)\n");
        return (NULL);
    }
    for (n = 0; n < no_servers; n++)
    {
        cap_mem[n] =  ((int)(max_mem[n] * cap) + 1);
        if (cap_mem[n] > max_mem[n])
        {
            cap_mem[n] = max_mem[n];
        }
        //fprintf(stdout, "%d ", cap_mem[n]);
    }
    return cap_mem;
}


int* get_cap_load(int no_servers, int* max_load)
{
    // data
    int n;
    int* cap_load;

    // code
    cap_load = (int*) malloc(sizeof(int) * no_servers);
    if (cap_load == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation (cap_load)\n");
        return (NULL);
    }
    for (n = 0; n < no_servers; n++)
    {
        cap_load[n] = (int) ((max_load[n] * cap) + 1);
        if (cap_load[n] > max_load[n])
        {
            cap_load[n] = max_load[n];
        }
    }
    return cap_load;
}


int* get_delta_memory(int no_servers, int* max_mem, int* cap_mem)
{
    // data
    int* deltaM;
    int n;

    // code
    deltaM = (int*) malloc(sizeof(int) * no_servers);
    if (deltaM == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation (cap_load)\n");
        return (NULL);
    }
    for (n = 0; n < no_servers; n++)
    {
        deltaM[n] = max_mem[n] - cap_mem[n];
        if (deltaM[n] < 0)
        {
            free(deltaM);
            fprintf(stderr, "[ERR] generated delta value negative\n");
            return (NULL);
        }
    }
    return deltaM;
}


int* get_delta_load(int no_servers, int* max_load, int* cap)
{
    // data
    int* delta;
    int n;

    // code
    delta = (int*) malloc(sizeof(int) * no_servers);
    if (delta == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation (cap_load)\n");
        return (NULL);
    }
    for (n = 0; n < no_servers; n++)
    {
        delta[n] = max_load[n] - cap[n];
        if (delta[n] < 0)
        {
            free(delta);
            fprintf(stderr, "[ERR] generated delta value negative\n");
            return (NULL);
        }
    }
    return delta;
}


int* fresh_node_list(int no_servers, int no_server_t, int* server_t)
{
    // data
    int* max_load;
    int n;
    int t;
    FILE* fp;

    // code
    printf("[INF] get fresh workload\n");
    max_load = (int*) malloc(sizeof(int) * no_servers);
    if (max_load == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error (max_load)\n");
        return (NULL);
    }
    // open file for later computing max power dissipation
    fp = fopen("../data/nodes.dat", "w");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] unable to open output file ('%s') for writing\n", "../data/nodes.dat");
        free(max_load);
        return NULL;
    }
    // generate the list of nodes with their associated type
    for (n = 0, t = 0; n < no_servers; n++)
    {
        if ((t % no_server_t == 0) && (t != 0))
        {
            t = 0;
        }
        max_load[n] = server_t[t];
        t++;
        // write on file the node types
        fprintf(fp, "%d\n", t);
    }
    // close the output file
    fclose(fp);
    return max_load;
}


int* get_server_types(int no_server_t)
{
    // data
    int t;
    int* server_t;
    char* line;
    int  i_val;
    int dummy;
    size_t line_size;
    FILE* fp;

    // code
    server_t = (int *) malloc(sizeof(int) * no_server_t);
    if (server_t == NULL)
    {
        fprintf(stderr, "[ERR] error allocating memory\n");
        return NULL;
    }
    // pre-initialization
    for (int t = 0; t < no_server_t; t++)
    {
        server_t[t] = -1;
    }
    // open input file
    fp = fopen(server_t_file, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] error opening input file (%s)\n", server_t_file);
        free(server_t);
        return NULL;
    }
    if (dbg)
    {
        fprintf(stdout, "[DBG] reading input file (%s)\n", server_t_file);
    }
    line = NULL;
    t = 0;
    while (getline(&line, &line_size, fp) != -1)
    {
        if (line[0] != '#')
        {
            sscanf(line, "%d : %d", &i_val, &dummy);
            server_t[t] = i_val;
            if (t == no_server_t)
            {
                break;
            }
            else
            {
                t++;
            }
        }
    }
    fclose(fp);
    dummy = dbg;
    dbg = false;
    if (dbg)
    {
        for (int i = 0; i < t; i++)
        {
            fprintf(stdout, "S-Type(%d) = %d\n", i, server_t[i]);
        }
    }
    dbg = dummy;
    return server_t;
}


int* get_server_Mtypes(int no_server_t)
{
    // data
    int t;
    int* server_m;
    char* line;
    int  i_val;
    int dummy;
    size_t line_size;
    FILE* fp;

    // code
    server_m = (int *) malloc(sizeof(int) * no_server_t);
    if (server_m == NULL)
    {
        fprintf(stderr, "[ERR] error allocating memory\n");
        return NULL;
    }
    // pre-initialization
    for (int t = 0; t < no_server_t; t++)
    {
        server_m[t] = -1;
    }
    // open input file
    fp = fopen(server_t_file, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] error opening input file (%s)\n", server_t_file);
        free(server_m);
        return NULL;
    }
    if (dbg)
    {
        fprintf(stdout, "[DBG] reading input file (%s)\n", server_t_file);
    }
    line = NULL;
    t = 0;
    while (getline(&line, &line_size, fp) != -1)
    {
        if (line[0] != '#')
        {
            sscanf(line, "%d : %d", &dummy, &i_val);
            server_m[t] = i_val;
            if (t == no_server_t)
            {
                break;
            }
            else
            {
                t++;
            }
        }
    }
    fclose(fp);
    return server_m;
}


int* get_task_Mtypes(int no_task_t)
{
    // data
    int t;
    int* task_m;
    char* line;
    int  i_val;
    int dummy;
    size_t line_size;
    FILE* fp;

    // code
    task_m = (int *) malloc(sizeof(int) * no_task_t);
    if (task_m == NULL)
    {
        fprintf(stderr, "[ERR] error allocating memory\n");
        return NULL;
    }
    // pre- initialization
    for (int t = 0; t < no_task_t; t++)
    {
        task_m[t] = -1;
    }
    // open input file
    fp = fopen(task_t_file, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] error opening input file (%s)\n", task_t_file);
        free(task_m);
        return NULL;
    }
    if (dbg)
    {
        fprintf(stdout, "[DBG] reading input file (%s)\n", task_t_file);
    }
    line = NULL;
    t = 0;
    while (getline(&line, &line_size, fp) != -1)
    {
        if (line[0] != '#')
        {
            sscanf(line, "%d : %d", &dummy, &i_val);
            task_m[t] = i_val;
            if (t == no_task_t)
            {
                fprintf(stderr, "** index = %d - boundary = %d\n", t, no_task_t);
                break;
            }
            else
            {
                t++;
            }
        }
    }
    fclose(fp);
    return task_m;
}


int* get_task_types(int no_task_t)
{
    // data
    int t;
    int* task_t;
    char* line;
    int  i_val;
    int dummy;
    size_t line_size;
    FILE* fp;

    // code
    task_t = (int *) malloc(sizeof(int) * no_task_t);
    if (task_t == NULL)
    {
        fprintf(stderr, "[ERR] error allocating memory\n");
        return NULL;
    }
    // pre- initialization
    for (int t = 0; t < no_task_t; t++)
    {
        task_t[t] = -1;
    }
    // open input file
    fp = fopen(task_t_file, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] error opening input file (%s)\n", task_t_file);
        free(task_t);
        return NULL;
    }
    if (dbg)
    {
        fprintf(stdout, "[DBG] reading input file (%s)\n", task_t_file);
    }
    line = NULL;
    t = 0;
    while (getline(&line, &line_size, fp) != -1)
    {
        if (line[0] != '#')
        {
            sscanf(line, "%d : %d", &i_val, &dummy);
            task_t[t] = i_val;
            if (t == no_task_t)
            {
                fprintf(stderr, "** index = %d - boundary = %d\n", t, no_task_t);
                break;
            }
            else
            {
                t++;
            }
        }
    }
    fclose(fp);
    return task_t;
}


//int** get_task_list (int population_size, int no_tasks, char* filename)
int* get_task_list (int no_tasks, char* filename)
{
    // data
    int p;
    int t;
    int s;
    //int** task_list;
    int* task_list;
    FILE* fp;

    // code
    task_list = (int*) malloc (sizeof(int) * no_tasks);
    if (task_list == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error (allocation of task_list vector)\n");
        return (NULL);
    }
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] error opening input task list file\n");
        free(task_list);
        return NULL;
    }
    for (t = 0; t < no_tasks; t++)
    {
        fscanf(fp, "%d", &s);
        task_list[t] = s;
    }
    fclose(fp);
    return task_list;
}


int* get_workload(int no_tasks, int* task_t, char* filename)
{
    // data
    int t;
    int w_t;
    int* workload;
    FILE* fp;

    // code
    workload = (int*) malloc(no_tasks * sizeof(int));
    if (workload == NULL)
    {
        return NULL;
    }
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        return NULL;
    }
    for (t = 0; t < no_tasks; t++)
    {
        fscanf(fp, "%d", &w_t);
        //workload[t] = task_t[w_t];
        workload[t] = w_t;
    }
    fclose(fp);
    return workload;
}


int** get_delta_server(int no_servers, int no_tasks, int population_size, int** task_list, int* workload, int* max_load, int* task_t)
{
    // data
    int p;
    int n;
    int t;
    int load;
    int** delta_server;


    // code
    delta_server = (int**) malloc(population_size * sizeof(int*));
    if (delta_server == NULL)
    {
        fprintf(stderr, "[ERR] error allocating memory (delta_server)\n");
        return NULL;
    }
    for (p = 0; p < population_size; p++)
    {
        delta_server[p] = (int*) malloc(no_servers * sizeof(int));
        if (delta_server[p] == NULL)
        {
            fprintf(stderr, "[ERR] error allocating memory (delta_server)\n");
            return NULL;
        }
        // calculate the delta of resources
        for (n = 0; n < no_servers; n++)
        {
            load = 0;
            for (t = 0; t < no_tasks; t++)
            {
                if (task_list[p][t] == n) {
                    load = load + task_t[workload[t]];
                }
            }
            delta_server[p][n] = max_load[n] - load;
        }
    }
    return delta_server;
}



int* get_mem_max_server(int no_servers, int* server_Mt, char* filename)
{
    // data
    int n;
    int s_t;
    int* max_mem;
    FILE* fp;

    // code
    max_mem = (int*) malloc(sizeof(int) * no_servers);
    if (max_mem == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error (max_mem)\n");
        return NULL;
    }
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        free(max_mem);
        fprintf(stderr, "[ERR] error opening input file (%s)\n", filename);
        return NULL;
    }
    for (n = 0; n < no_servers; n++)
    {
        fscanf(fp, "%d", &s_t);
        max_mem[n] = server_Mt[s_t - 1];
        //fprintf(stdout, "memory => %d\n", max_mem[n]);
    }
    fclose(fp);
    return max_mem;
}




int* get_max_server(int no_servers, int* server_t, char* filename)
{
    // data
    int n;
    int s_t;
    int* max_load;
    FILE* fp;

    // code
    max_load = (int*) malloc(sizeof(int) * no_servers);
    if (max_load == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error (max_load)\n");
        return NULL;
    }
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        free(max_load);
        fprintf(stderr, "[ERR] error opening input file (%s)\n", filename);
        return NULL;
    }
    for (n = 0; n < no_servers; n++)
    {
        fscanf(fp, "%d", &s_t);
        max_load[n] = server_t[s_t - 1];
    }
    fclose(fp);
    return max_load;
}


int get_config(int* population_size, int* no_tasks, int* no_servers, int* no_task_t, int* no_server_t, char* filename)
{
    // data
    FILE *fp;
    char* line;
    char* token;
    int  i_val;
    double f_val;
    size_t line_size;

    // code
    // open configuration file
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        return FAILURE;
    }
    // get configuration
    line = NULL;
    while (getline(&line, &line_size, fp) != -1)
    {
        if (line[0] != '#')
        {
            token = strtok(line,"=");
            // evaluate the parameters
            if (strcmp(token, "dbg") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%d", &i_val);
                dbg = i_val;
            }
            if (strcmp(token, "population") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%d", &i_val);
                *population_size = i_val;
            }
            if (strcmp(token, "no_tasks") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%d", &i_val);
                *no_tasks = i_val;
            }
            if (strcmp(token, "no_servers") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%d", &i_val);
                *no_servers = i_val;
            }
            if (strcmp(token, "no_task_t") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%d", &i_val);
                *no_task_t = i_val;
            }
            if (strcmp(token, "no_server_t") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%d", &i_val);
                *no_server_t = i_val;
            }
            if (strcmp(token, "gen_workload") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%d", &i_val);
                if (i_val > 0)
                    gen_workload = true;
                else
                    gen_workload = false;
            }
            if (strcmp(token, "cap") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%lf", &f_val);
                cap = f_val;
            }
            if (strcmp(token, "generations") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%d", &i_val);
                generations = i_val;
            }
            if (strcmp(token, "max_time") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%lf", &f_val);
                max_time = f_val;
            }
            if (strcmp(token, "mu_1") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%lf", &f_val);
                mu_1 = f_val;
            }
            if (strcmp(token, "mu_2") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%lf", &f_val);
                mu_2 = f_val;
            }
            if (strcmp(token, "mu_3") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%lf", &f_val);
                mu_3 = f_val;
            }
            if (strcmp(token, "mu_4") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%lf", &f_val);
                mu_4 = f_val;
            }
            if (strcmp(token, "mu_5") == 0)
            {
                token = strtok(NULL, "=\n");
                sscanf(token, "%lf", &f_val);
                mu_5 = f_val;
            }
        }
    }
    fclose(fp);
    return SUCCESS;
}



double* get_max_power_dc(int no_servers, int no_server_t, char* filename, char* filename2)
{
    // data
    double* power_server;
    double* power_dc;
    double val;
    int i_val;
    int n;
    FILE* fp;

    // code
    power_server = (double*) malloc(sizeof(double) * no_server_t);
    power_dc = (double*) malloc(sizeof(double) * no_servers);
    if (power_dc == NULL || power_server == NULL)
    {
        fprintf(stderr, "[ERR] memory allocation error\n");
        free(power_dc);
        free(power_server);
        return NULL;
    }
    // reading per server type power consumption (filename)
    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] opening input file\n");
        free(power_dc);
        free(power_server);
        return NULL;
    }
    fprintf(stdout, "[DBG] no. of server types = %d\n", no_server_t);
    for (n = 0; n < no_server_t; n++)
    {
        fscanf(fp, "%lf", &val);
        power_server[n] = val;
    }
    fclose(fp);
    //
    // reading the data center server composition (filename2)
    fp = fopen(filename2, "r");
    if (fp == NULL)
    {
        fprintf(stderr, "[ERR] opening input file\n");
        free(power_dc);
        free(power_server);
        return NULL;
    }
    fprintf(stdout, "[DBG] no. of servers = %d\n", no_servers);
    for (n = 0; n < no_servers; n++)
    {
        fscanf(fp, "%d", &i_val);
        power_dc[n] = power_server[i_val - 1];
    }
    fclose(fp);
    // freeing the power server array
    free(power_server);
    return power_dc;
}



int** release_task_list(int population_size, int** task_list)
{
    // data
    int p;

    // code
    for (p = 0; p < population_size; p++)
    {
        free(task_list[p]);
    }
    free(task_list);
    return NULL;
}


int* release_workload(int* workload)
{
    // code
    free(workload);
    return NULL;
}


int** release_delta_server(int population_size, int** delta_server)
{
    // data
    int p;

    // code
    for (p = 0; p < population_size; p++)
    {
        free(delta_server[p]);
    }
    free(delta_server);
    return NULL;
}


int* release_max_server(int* max_load)
{
    // code
    free (max_load);
    return NULL;
}


double* release_power_dc(double* power_dc)
{
    // code
    free (power_dc);
    return NULL;
}


int schedule_BF(int no_servers,
                int no_tasks,
                int no_task_t,
                int no_server_t,
                int* cap_load,
                int* cap_mem,
                int* task_t,
                int* task_Mt,
                int* task_list,
                int* workload)
{
    // data
    int* task_counters;
    int* delta;
    int* deltaM;
    int* schedule;
    int t;
    int tt;
    int n;
    int nt;
    int s;
    int bool_1;
    int bool_2;
    int bool_3;
    int set;
    int task;
    int taskM;
    int dummy_dbg;
    int go;
    int* server_type_list;
    FILE* f;

    // code
    fprintf(stdout, "[BFS] number of server types: %d\n", no_server_t);
    server_type_list = (int*) malloc(sizeof(int) * no_servers);
    schedule = (int*) malloc(sizeof(int) * no_tasks);
    task_counters = (int*) malloc(sizeof(int) * no_task_t);
    delta = (int*) malloc(sizeof(int) * no_servers);
    deltaM = (int*) malloc(sizeof(int) * no_servers);
    if (task_counters == NULL || delta == NULL || deltaM == NULL || schedule == NULL || server_type_list == NULL)
    {
        fprintf(stderr, "[ERR] unable to allocate memory for best fit scheduling policy\n");
        return FAILURE;
    }
    // reading the server type list
    f = fopen("../data/nodes.dat", "r");
    if (f == NULL)
    {
        fprintf(stderr, "[ERR] unable to open file: BF scheduling policy aborted\n");
        free(server_type_list);
        free(schedule);
        free(task_counters);
        free(delta);
        free(deltaM);
        return FAILURE;
    }
    for (s = 0; s < no_servers; s++)
    {
        fscanf(f, "%d", &server_type_list[s]);
        server_type_list[s]--;
    }
    // check
    dummy_dbg = dbg;
    dbg = false;
    if (dbg)
    {
        fprintf(stdout, "[DBG] server type list:\n");
        for (s = 0; s < no_servers; s++)
        {
            if ((s % 25 == 0) && (s > 0))
            {
                fprintf(stdout, "\n");
            }
            fprintf(stdout, "%2d ", server_type_list[s]);
        }
        fprintf(stdout, "\n");
    }
    dbg = dummy_dbg;
    fclose(f);
    // resetting data structures
    for (t = 0; t < no_task_t; t++)
    {
        task_counters[t] = 0;
    }
    for (n = 0; n < no_servers; n++)
    {
        delta[n] = cap_load[n];
        deltaM[n] = cap_mem[n];
    }
    // acquire statistics on the initial placement
    for (t = 0; t < no_tasks; t++)
    {
        task_counters[workload[t]]++;
        schedule[t] = -1;
    }
    dummy_dbg = dbg;
    dbg = true;
    if (dbg)
    {
        fprintf(stdout, "[BFS] statistics (task types distribution):\n");
        for (t = 0; t < no_task_t; t++)
        {
            fprintf(stdout, "      task_class [ %2d ] = %d\n", t, task_counters[t]);
        }
    }
    dbg = dummy_dbg;
    // run the schedule policy: starting from larger task types tries to allocate on the
    // server with larger available resources (capped ones). The idea is to force the
    // procedure to start using larger servers and than smallest ones.
    go = true;
    while (go == true)
    {
        go = false;
        //for (tt = 0; tt < no_task_t; tt++)
        for (tt = no_task_t - 1; tt >= 0; tt--)
        {
            if (task_counters[tt] > 0)
            {
                go = true;
                break;
            }
        }
        // select the next task of type 'tt' => t
        for (t = 0; t < no_tasks; t++)
        {
            if (workload[t] == tt)
                break;
        }
        task = task_t[workload[t]];
        taskM = task_Mt[workload[t]];
        // select the server for the assignment
        set = false;
        for(nt = 0; nt < no_server_t && set == false; nt++)
        {
            s = 0;
            set = false;
            for (n = 0; n < no_servers; n++)
            {
                if (server_type_list[n] == nt)
                {
                    bool_1 = delta[n] >= task;
                    bool_2 = deltaM[n] >= taskM;
                    bool_3 = (delta[n] > delta[s]) || (deltaM[n] > deltaM[s]);
                    if (bool_1 && bool_2 && bool_3)
                    {
                        s = n;
                        set = true;
                    }
                }
            }
            if (set)
            {
                delta[s] -= task;
                deltaM[s] -= taskM;
                schedule[t] = s;
            }
        }
        if (set == false)
        {
            go = false;
            fprintf(stdout, "[WAR] the best fit procedure is no longer able to assign all the tasks\n");
        }
    }
    // freeing resources
    free(schedule);
    free(task_counters);
    free(delta);
    free(deltaM);
    return SUCCESS;
}

