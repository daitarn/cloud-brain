# Minimal CLI interface for the Load-Balancer algorithm
# The purpose of the algorithm is to consolidate at runtime the DC workload reducing
# the amount of power consumed (and thus the energy)


# Other parameters
SRC := ./src/*.c
BIN_SIM := dc_solver
# ----------------
# Commands
# ----------------
# packing all the files in a zip archive
packing:
	@echo
	@echo "[pkg] getting current timestamp";
	@echo "[pkg] create compressed archive";
	@zip -q -r "./archives/cloud_brain__$(shell date +%s).zip" ./bin/ ./src/ Makefile;
	@echo


# compile the code
build:
	@echo; \
	echo "[inf] building simulation infrastructure"; \
	gcc -std=c11 -lpthread -O2 -o ./bin/$(BIN_SIM) $(SRC) \
		-lm 2> ./tmp/build.log; \
	if [ $$? -eq 0 ]; then \
		echo "[inf] successful build $$?"; \
		rm ./tmp/build.log; \
	else \
		echo "[err] process terminated with exit_code ($$?)"; \
		echo "[err] building errors"; \
	fi;
	@echo

# run the simulation
run:
	@echo
	@echo "[inf] starting simulation infrastructure"
	@echo "[inf] check for loading database"
	@cd ./bin/ &&\
	pwd && \
	./$(BIN_SIM) && \
	cd ../
	@echo

# run the plotting system
plot:
	@echo
	@echo "[inf] plotting results and statistics"
	@cd ./bin/ &&\
	./fitness_plot.gp &&\
	cd ../
	@echo


# clean up the installation
remove_bin:
	@echo "[inf] removing the engine installation"
	@rm ./bin/$(BIN_SIM) 


