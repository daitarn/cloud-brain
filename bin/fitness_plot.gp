#! /usr/bin/gnuplot
set term png size 1024,720 font "/usr/share/fonts/liberation/LiberationSans-Regular.ttf" 12
set output '../plots/fitness_track.png'

set xlabel 'Generations'
set ylabel 'Schedule efficiency [W]'

set grid
set key below center horizontal noreverse enhanced autotitle
set tics out nomirror

set xrange [0:18851]
set xtics rotate
set yrange [180170:278784]

set style line 1 linecolor rgb '#99004c' linewidth 1 linetype 3 pointtype 2
set style data linespoints

plot '../data/es_dump.txt' using 1:2 linestyle 1 title 'Schedule efficiency'
